//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqNerscLoginBehavior_h
#define pqNerscLoginBehavior_h

// ParaView includes
#include "pqReaction.h"

// Qt includes
#include <QObject>

namespace newt
{
class qtNewtLoginDialog;
}

//\brief A reaction for user login to NERSC
// The current implementation only permits a single login to NERSC
// each session. Users must close modelbuilder if they want to login
// to NERSC as a different user.
class pqNerscLoginReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqNerscLoginReaction(QAction* parent);

Q_SIGNALS:
  void nerscLogin(QString username);
  void nerscLogout();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqNerscLoginReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqNerscLoginBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqNerscLoginBehavior* instance(QObject* parent = nullptr);
  ~pqNerscLoginBehavior() override;

  // Link back to menu so that we can update text
  void setMenuAction(QAction* action) { m_action = action; }

public Q_SLOTS:
  // Opens the login dialog to begin authentication
  void startLogin();

protected Q_SLOTS:
  // Reacts to successful login to NERSC
  void onLoginComplete(const QString userName);

  // Reacts to credentials provided to the login dialog
  void onLoginCredentials(const QString userName, const QString password);

  // Reacts to failed login attempt
  void onLoginError(const QString message);
  void onLoginFail(const QString message);

protected:
  pqNerscLoginBehavior(QObject* parent = nullptr);

  bool m_isLoggedIn;
  QAction* m_action;
  newt::qtNewtLoginDialog* m_loginDialog;

private:
  Q_DISABLE_COPY(pqNerscLoginBehavior);
};

#endif
