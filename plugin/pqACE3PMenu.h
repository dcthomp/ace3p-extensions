//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PMenu_h
#define pqACE3PMenu_h

#include "smtk/PublicPointerDefs.h"

#include <QActionGroup>

class QAction;

class pqServer;

class pqACE3PRecentProjectsMenu;
class pqACE3PRemoteParaViewMenu;

/** \brief Adds the "ACE3P" menu to the application main window
  */
class pqACE3PMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

public Q_SLOTS:
  void onProjectOpened(smtk::project::ProjectPtr project);
  void onProjectClosed();

public:
  pqACE3PMenu(QObject* parent = nullptr);
  ~pqACE3PMenu() override;

protected:
  bool startup();
  void shutdown();

protected Q_SLOTS:

  /** \brief Sets visibility of NERSC and Amazon EC2 menu items.
   *
   * The NERSC and EC2 menu items are enabled in the ACE3P settings,
   * which can be accessed after the builtin server has connected.
  */
  void finalizeMenu(pqServer* server);

private:
  // In order they appear in the ACE3P menu:
  QAction* m_openProjectAction = nullptr;
  QAction* m_recentProjectsAction = nullptr;
  QAction* m_newProjectAction = nullptr;
  QAction* m_newStageAction = nullptr;
  QAction* m_saveProjectAction = nullptr;
  QAction* m_closeProjectAction = nullptr;
  QAction* m_nerscLoginAction = nullptr;
  QAction* m_nerscSubmitAction = nullptr;
  QAction* m_amazonListClustersAction = nullptr;
  QAction* m_amazonSubmitAction = nullptr;

  // Not displayed in the menu
  QAction* m_nerscBrowserAction = nullptr;

  pqACE3PRecentProjectsMenu* m_recentProjectsMenu = nullptr;
  pqACE3PRemoteParaViewMenu* m_nerscParaViewMenu = nullptr;
  bool m_saveMenuConfigured = false;

  // Store new-server connection so that we can disconnect later.
  QMetaObject::Connection m_serverConnection;

  Q_DISABLE_COPY(pqACE3PMenu);
};

#endif // pqACE3PMenu_h
