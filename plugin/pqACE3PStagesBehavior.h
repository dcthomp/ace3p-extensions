//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PStagesBehavior_h
#define pqACE3PStagesBehavior_h

#include "pqReaction.h"

#include "smtk/PublicPointerDefs.h"

#include <QObject>

#include <memory>

class pqACE3PNewStageBehavior;
class pqACE3PStagesPanel;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class Project;
}
} // namespace simulation
} // namespace smtk

/// A reaction for closing a project.
class pqACE3PStagesReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3PStagesReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PStagesReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqACE3PStagesBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;
  friend class pqACE3PNewStageBehavior;
  friend class pqACE3PStagesPanel;

public Q_SLOTS:
  void selectStage(int index);

  /** \brief Hides the Stages Panel - a special case of setPanelVisible(). */
  void hideStagesPanel();

public:
  static pqACE3PStagesBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PStagesBehavior() override;

  /** \brief Sets pointer for the Stages Panel (used to change its visibility). */
  void setStagesPanelPointer(pqACE3PStagesPanel* stagesPanel) { m_stagesPanel = stagesPanel; }

  //bool setPanelVisibility(QString panelName, bool bVisible);

  /** \brief Shows/hides Stages widget panel */
  void setPanelVisible(bool bVisible);

  /** \brief Shows/hides Stages widget panel based on the number of Stages in current Project.*/
  void updateStagesPanelVisibility(std::shared_ptr<smtk::simulation::ace3p::Project> ace3pProject);

  /** \brief Should be called when project first loaded, to set current stage */
  void initStage(int index);

Q_SIGNALS:
  void stageSelected(int index);

protected:
  pqACE3PStagesBehavior(QObject* parent = nullptr);

  void setStage(int index, bool updateProject);
  void setModelVisibility(
    std::shared_ptr<smtk::simulation::ace3p::Project> ace3pProject,
    int index);

private:
  Q_DISABLE_COPY(pqACE3PStagesBehavior);

  pqACE3PStagesPanel* m_stagesPanel;
};

#endif
