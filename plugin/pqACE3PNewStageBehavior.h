//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_plugin_pqACE3PNewStageBehavior_h
#define smtk_simulation_ace3p_plugin_pqACE3PNewStageBehavior_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include "pqReaction.h"

#include <QObject>

class pqACE3PNewStageBehaviorInternals;

/** \brief A reaction for creating a new ACE3P Stage. */
class pqACE3PNewStageReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqACE3PNewStageReaction(QAction* parent);
  ~pqACE3PNewStageReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PNewStageReaction)
};

/** \brief Creates new ACE3P Stage. */
class pqACE3PNewStageBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public Q_SLOTS:
  void createNewStage();

public:
  static pqACE3PNewStageBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PNewStageBehavior() override;

Q_SIGNALS:
  void stageAdded(int stageNumber);

protected Q_SLOTS:
  void onOperationExecuted(const smtk::operation::Operation::Result& result);

protected:
  pqACE3PNewStageBehavior(QObject* parent = nullptr);

private:
  pqACE3PNewStageBehaviorInternals* Internals;

  Q_DISABLE_COPY(pqACE3PNewStageBehavior);
};

#endif
