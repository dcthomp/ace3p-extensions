//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PStagesBehavior.h"

// ACE3P
#include "plugin/pqACE3PAutoStart.h"
#include "plugin/pqACE3PStagesPanel.h"
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/common/UUID.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKAttributePanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/model/Resource.h"
#include "smtk/resource/Resource.h"

// Client side
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqPipelineRepresentation.h"
#include "pqPipelineSource.h"
#include "pqRenderView.h"
#include "pqRepresentation.h"
#include "pqServer.h"
#include "pqServerManagerModel.h"

#include <QDebug>
#include <QList>
#include <QSet>

//-----------------------------------------------------------------------------
pqACE3PStagesReaction::pqACE3PStagesReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqACE3PStagesReaction::onTriggered()
{
  // Not currently used
}

//-----------------------------------------------------------------------------
static pqACE3PStagesBehavior* g_instance = nullptr;

//-----------------------------------------------------------------------------
pqACE3PStagesBehavior::pqACE3PStagesBehavior(QObject* parent)
  : Superclass(parent)
  , m_stagesPanel(nullptr)
{
}

//-----------------------------------------------------------------------------
pqACE3PStagesBehavior* pqACE3PStagesBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqACE3PStagesBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

//-----------------------------------------------------------------------------
pqACE3PStagesBehavior::~pqACE3PStagesBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqACE3PStagesBehavior::hideStagesPanel()
{
  this->setPanelVisible(false);
}

void pqACE3PStagesBehavior::setPanelVisible(bool bVisible)
{
  m_stagesPanel->setVisible(bVisible);
}

void pqACE3PStagesBehavior::updateStagesPanelVisibility(
  std::shared_ptr<smtk::simulation::ace3p::Project> ace3pProject)
{
  bool bStagePanelVisible = ace3pProject->numberOfStages() > 1;
  this->setPanelVisible(bStagePanelVisible);
}

//-----------------------------------------------------------------------------
void pqACE3PStagesBehavior::initStage(int index)
{
  // Make sure that smtk UI elements are initialized
  pqCoreUtilities::processEvents();
  this->setStage(index, false);
}

//-----------------------------------------------------------------------------
void pqACE3PStagesBehavior::selectStage(int index)
{
  this->setStage(index, true);
}

//-----------------------------------------------------------------------------
void pqACE3PStagesBehavior::setStage(int index, bool updateProject)
{
  // Get current project
  auto* runtime = smtk::simulation::ace3p::qtProjectRuntime::instance();
  std::shared_ptr<smtk::simulation::ace3p::Project> ace3pProject = runtime->ace3pProject();
  if (!ace3pProject)
  {
    qWarning() << "Internal error - no active ace3p project." << __FILE__ << __LINE__;
    return;
  }

  if (index < 0 || index >= ace3pProject->numberOfStages())
  {
    qWarning() << "Internal error - invalid stage index." << __FILE__ << __LINE__;
    return;
  }

  // Trigger update of Attribute Panel
  auto attrResource = ace3pProject->stage(index)->attributeResource();
  if (attrResource == nullptr)
  {
    qWarning() << "Internal error - stage" << index << "missing attribiute resource" << __FILE__
               << __LINE__;
    return;
  }

  // Find the attribute panel and set it to display the stage's resource.
  QWidget* mainWidget = pqCoreUtilities::mainWidget();
  if (!mainWidget)
  {
    qWarning() << "Internal error - no main widget." << __FILE__ << __LINE__;
    return;
  }

  QDockWidget* attributeDock = mainWidget->findChild<QDockWidget*>("pqSMTKAttributeDock");
  if (attributeDock == nullptr)
  {
    qWarning() << "Unable to find Attribute Editor dock widget (pqSMTKAttributeDock)";
  }
  else
  {
    QWidget* widget = attributeDock->widget();
    pqSMTKAttributePanel* attrPanel = dynamic_cast<pqSMTKAttributePanel*>(widget);
    if (attrPanel)
    {
      attrPanel->displayResourceOnServer(attrResource);
    }
    else
    {
      qWarning() << "pqSMTKAttributeDock missing pqSMTKAttributePanel widget";
    }
  }

  this->setModelVisibility(ace3pProject, index);

  // Update the project
  if (updateProject)
  {
    ace3pProject->setCurrentStage(index, true); // marks the project as dirty
  }

  Q_EMIT this->stageSelected(index); // currently this signal doesn't seem to be used for anything
}

//-----------------------------------------------------------------------------
void pqACE3PStagesBehavior::setModelVisibility(
  std::shared_ptr<smtk::simulation::ace3p::Project> ace3pProject,
  int index)
{
  // Get model resource for this stage
  auto currentModelRes = ace3pProject->stage(index)->modelResource();
  if (currentModelRes == nullptr)
  {
    qWarning() << "Stage" << index << "has no model resource. Not updating renderviews";
    return;
  }

  // Create QSet of model resources from all stages
  QSet<smtk::common::UUID> otherModelResources; // use uuid because QSet cannot has resourceptr
  for (int i = 0; i < ace3pProject->numberOfStages(); ++i)
  {
    otherModelResources += ace3pProject->stage(i)->modelResource()->id();
  }
  otherModelResources -= currentModelRes->id(); // remove current stage's model

  // Update all render views connected to builtin server
  pqServer* builtinServer = pqACE3PAutoStart::builtinServer();
  pqServerManagerModel* smModel = pqApplicationCore::instance()->getServerManagerModel();
  QList<pqRenderView*> viewList = smModel->findItems<pqRenderView*>(builtinServer);
  Q_FOREACH (pqRenderView* view, viewList)
  {
    bool modelsVisible = false;
    pqRepresentation* currentModelRep = nullptr;

    // Traverse representations
    QList<pqRepresentation*> repList = view->getRepresentations();
    for (auto rep : repList)
    {
      auto* pipelineRep = dynamic_cast<pqPipelineRepresentation*>(rep);
      if (pipelineRep == nullptr)
      {
        continue; // ignore any objects that isn't a pqPipelineRepresentation
      }

      pqPipelineSource* source = pipelineRep->getInput();
      if (source == nullptr)
      {
        continue; // ignore any representation with not source
      }

      pqSMTKResource* smtkSource = dynamic_cast<pqSMTKResource*>(source);
      if (smtkSource == nullptr)
      {
        continue; // ignore any source that is not an SMTK source
      }

      smtk::resource::ResourcePtr smtkResource = smtkSource->getResource();
      if (smtkResource == nullptr)
      {
        // Sanity check that source is connected to an smtk resource.
        qWarning() << "Empty pqSMTKResource";
      }

      smtk::model::ResourcePtr smtkModelResource =
        std::dynamic_pointer_cast<smtk::model::Resource>(smtkResource);
      if (smtkModelResource == nullptr)
      {
        continue; // ignore sources that are not smtk model resources
      }

      if (smtkModelResource == currentModelRes)
      {
        currentModelRep = rep; // remember there is a rep connected to the model resource
        continue;
      }

      if (otherModelResources.contains(smtkModelResource->id()) && rep->isVisible())
      {
        modelsVisible = true;   // models are visible in this view
        rep->setVisible(false); // hide the current model
      }
    }

    if (currentModelRep == nullptr)
    {
      continue;
    }

    // If models are visible in this view or if the view is empty,
    // then make the current stage's model visible.
    bool showModel = modelsVisible || (view->getNumberOfVisibleRepresentations() < 1);
    if (showModel)
    {
      currentModelRep->setVisible(true);
      view->render();
    }
  } // foreach (renderview)
}
