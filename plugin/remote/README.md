# Remote Server Configuration Files

The file `modelbuilder-cori.pvsc` is used to configure modelbuilder for
connecting to ParaView servers on NERSC/Cori. It is an updated version of
the NERSC configuration files, `cori-unix.pvsc` and `cori-win.pvsc`, which
were downloaded from https://docs.nersc.gov/applications/paraview/ and are
included for reference only.

The `modelbuilder-cori.pvsc` file uses syntax introduced in ParaView 5.7
and described in this discourse topic:
https://discourse.paraview.org/t/using-ssh-support-to-secure-your-client-server-communication-with-paraview/682
The new syntax works across all 3 platforms and no longer requires X11
to be install on macOS systems.
