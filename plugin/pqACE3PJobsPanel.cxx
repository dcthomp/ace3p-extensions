//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "pqACE3PJobsPanel.h"
#include "pqACE3PJobLoader.h"

#include "pqActiveObjects.h"
#include "pqApplicationCore.h"

#include "plugin/pqACE3PCloseBehavior.h"
#include "plugin/pqACE3PExportBehavior.h"
#include "plugin/pqACE3PJobsBehavior.h"
#include "plugin/pqACE3PNewProjectBehavior.h"
#include "plugin/pqACE3PProjectLoader.h"
#include "plugin/pqNerscFileBrowserBehavior.h"

#include "smtk/newt/qtNewtFileBrowserWidget.h"
#include "smtk/simulation/ace3p/qt/qtJobsWidget.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"

#include <QDebug>

pqACE3PJobsPanel::pqACE3PJobsPanel(QWidget* parent)
  : QDockWidget(parent)
  , m_jobWidget(nullptr)
{
  this->setObjectName("Jobs Panel");
  this->setWindowTitle("Simulation Jobs");

  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "pqACE3PJobsPanel missing pqApplicationCore";
    return;
  }

  auto jobsBehavior = pqACE3PJobsBehavior::instance(this);
  m_jobWidget = jobsBehavior->jobsWidget();
  this->setWidget(m_jobWidget);

  jobsBehavior->setJobsPanelPointer(this);

  auto smtkBehavior = pqSMTKBehavior::instance();
  // Now listen for future connections.
  QObject::connect(
    smtkBehavior,
    SIGNAL(removingManagerFromServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(sourceRemoved(pqSMTKWrapper*, pqServer*)));

  auto creater = pqACE3PNewProjectBehavior::instance();
  QObject::connect(
    creater,
    &pqACE3PNewProjectBehavior::projectCreated,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::setProject);

  auto loader = pqACE3PProjectLoader::instance();
  QObject::connect(
    loader,
    &pqACE3PProjectLoader::projectOpened,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::setProject);

  auto exporter = pqACE3PExportBehavior::instance();

  auto closer = pqACE3PCloseBehavior::instance();
  QObject::connect(
    closer,
    &pqACE3PCloseBehavior::projectClosed,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::onProjectClosed);
  QObject::connect(
    exporter,
    &pqACE3PExportBehavior::jobSubmitted,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::onJobSubmitted);

  // connect navigation buttons to nersc file browser
  auto nerscBrowserInstance = pqNerscFileBrowserBehavior::instance();
  auto nerscBrowserWidget = nerscBrowserInstance->getBrowser();
  QObject::connect(
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::requestNavigateDir,
    nerscBrowserWidget,
    &newt::qtNewtFileBrowserWidget::onRequestNavigate);

  auto jobLoader = pqACE3PJobLoader::instance();
  QObject::connect(
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::requestLoadJob,
    jobLoader,
    &pqACE3PJobLoader::onRequestLoadJob);

  // hide the panel upon startup (only really required once - upon very first program load)
  QTimer::singleShot(50, jobsBehavior, &pqACE3PJobsBehavior::hideJobsPanel);
}

pqACE3PJobsPanel::~pqACE3PJobsPanel()
{
  auto pqCore = pqApplicationCore::instance();
  pqCore->unRegisterManager(QString("analysis_panel"));
}

void pqACE3PJobsPanel::infoSlot(const QString& msg)
{
  // Would like to emit signal to main window status bar
  // but that is not currently working. Instead, qInfo()
  // messages are sent to the CMB Log Window:
  qInfo() << "pqACE3PJobsPanel:" << msg;
}

void pqACE3PJobsPanel::sourceRemoved(pqSMTKWrapper* mgr, pqServer* server)
{
  if (!mgr || !server)
  {
    return;
  }
}
