set(client_headers
  pqACE3PAutoStart.h
  pqACE3PCloseBehavior.h
  pqACE3PExportBehavior.h
  pqACE3PJobLoader.h
  pqACE3PJobsBehavior.h
  pqACE3PJobsPanel.h
  pqACE3PMenu.h
  pqACE3PNewProjectBehavior.h
  pqACE3PNewStageBehavior.h
  pqACE3POpenBehavior.h
  pqACE3PPluginLocation.h
  pqACE3PProjectLoader.h
  pqACE3PRecentProjectsMenu.h
  pqACE3PRemoteParaViewBehavior.h
  pqACE3PSaveBehavior.h
  pqACE3PStagesBehavior.h
  pqACE3PStagesPanel.h

  pqCubitToolBar.h
  # pqCumulusJobsPanel.h

  pqNerscFileBrowserBehavior.h
  pqNerscFileBrowserPanel.h
  pqNerscLoginBehavior.h
)

set(client_sources
  pqACE3PAutoStart.cxx
  pqACE3PCloseBehavior.cxx
  pqACE3PExportBehavior.cxx
  pqACE3PJobLoader.cxx
  pqACE3PJobsBehavior.cxx
  pqACE3PJobsPanel.cxx
  pqACE3PMenu.cxx
  pqACE3PNewProjectBehavior.cxx
  pqACE3PNewStageBehavior.cxx
  pqACE3POpenBehavior.cxx
  pqACE3PPluginLocation.cxx
  pqACE3PProjectLoader.cxx
  pqACE3PRecentProjectsMenu.cxx
  pqACE3PRemoteParaViewBehavior.cxx
  pqACE3PSaveBehavior.cxx
  pqACE3PStagesBehavior.cxx
  pqACE3PStagesPanel.cxx

  pqCubitToolBar.cxx
  # pqCumulusJobsPanel.cxx

  pqNerscFileBrowserBehavior.cxx
  pqNerscFileBrowserPanel.cxx
  pqNerscLoginBehavior.cxx
)

# Generate remote pvserver config file
set(machines "cori")
foreach (machine ${machines})
  set(pvsc_filename "modelbuilder-${machine}.pvsc")
  if (WIN32)
    set(pvsc_filename "modelbuilder-${machine}-win.pvsc")
  endif ()
  set (pvsc_source "${CMAKE_CURRENT_SOURCE_DIR}/remote/${pvsc_filename}")
  smtk_encode_file(${pvsc_source} NAME "${machine}_pvsc" TYPE "_cpp"
    HEADER_OUTPUT pvsc_target)
  list(APPEND client_headers "${pvsc_target}")
endforeach ()

set(CMAKE_AUTOMOC 1)

# Auto start class
paraview_plugin_add_auto_start(
  CLASS_NAME pqACE3PAutoStart
  INTERFACES auto_start_interfaces
  SOURCES auto_start_sources
)

# Plugin location
paraview_plugin_add_location(
    CLASS_NAME pqACE3PPluginLocation
    INTERFACES location_interfaces
    SOURCES location_sources
  )

# ACE3P menu
paraview_plugin_add_action_group(
  CLASS_NAME pqACE3PMenu
  GROUP_NAME "MenuBar/ACE3P"
  INTERFACES menu_interfaces
  SOURCES menu_sources
)

paraview_plugin_add_dock_window(
   CLASS_NAME pqACE3PStagesPanel
   DOCK_AREA Left
   INTERFACES stage_dock_interfaces
   SOURCES stage_dock_sources
 )

paraview_plugin_add_dock_window(
   CLASS_NAME pqACE3PJobsPanel
   DOCK_AREA Left
   INTERFACES analysis_dock_interfaces
   SOURCES analysis_dock_sources
 )

# Specify the NERSC browser dock widget
paraview_plugin_add_dock_window(
  CLASS_NAME pqNerscFileBrowserPanel
  DOCK_AREA Right
  INTERFACES browser_dock_interfaces
  SOURCES browser_dock_sources
)

# Jobs panel dock widget
# paraview_plugin_add_dock_window(
#   CLASS_NAME pqCumulusJobsPanel
#   DOCK_AREA Left
#   INTERFACES jobs_dock_interfaces
#   SOURCES jobs_dock_sources
# )

# CUBIT toolbar
paraview_plugin_add_toolbar(
  CLASS_NAME pqCubitToolBar
  INTERFACES toolbar_interfaces
  SOURCES toolbar_sources
)

# ACE3P plugin
paraview_add_plugin(smtkACE3PPlugin
  VERSION "1.0"
  UI_INTERFACES
    ${auto_start_interfaces}
    ${location_interfaces}
    ${browser_dock_interfaces}
    ${jobs_dock_interfaces}
    ${menu_interfaces}
    ${toolbar_interfaces}
    ${analysis_dock_interfaces}
    ${stage_dock_interfaces}
  SOURCES
    ${client_headers}
    ${client_sources}
    ${auto_start_sources}
    ${location_sources}
    ${browser_dock_sources}
    ${jobs_dock_sources}
    ${menu_sources}
    ${toolbar_sources}
    ${analysis_dock_sources}
    ${stage_dock_sources}
    ${rpJsonHeader}
  UI_RESOURCES
    icons/icons.qrc
  SERVER_MANAGER_XML
    ACE3PSettings.xml
  XML_DOCUMENTATION OFF
)

target_compile_definitions(smtkACE3PPlugin
  PRIVATE
    "WORKFLOWS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/simulation-workflows\""
)

if (ENABLE_ACE3P_UI_FEATURES)
  target_compile_definitions(smtkACE3PPlugin PRIVATE ENABLE_ACE3P_UI_FEATURES)
endif ()

target_link_libraries(smtkACE3PPlugin
  LINK_PUBLIC
    smtkACE3PQtExt
    smtkACE3P
    smtkNewt
    # smtkCumulus
    smtkCore
    smtkPQComponentsExt
    smtkPVServerExt
    ParaView::pqApplicationComponents
    ParaView::pqComponents
    Qt5::Core
)
if (ParaView_VERSION VERSION_LESS "5.10.0")
  list(APPEND smtkACE3PPlugin_extra_defs PARAVIEW_VERSION_59)
endif ()
target_compile_definitions(smtkACE3PPlugin PRIVATE QT_NO_KEYWORDS ${smtkACE3PPlugin_extra_defs})

target_include_directories(smtkACE3PPlugin PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
)
target_compile_definitions(
  smtkACE3PPlugin PUBLIC RECENTLY_USED_PROJECTS_TAG="SMTK_PROJECT"
)
