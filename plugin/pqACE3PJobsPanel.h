//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME pqACE3PJobsPanel - display jobs on remote system
// .SECTION Description

#ifndef plugin_pqACE3PJobsPanels_h
#define plugin_pqACE3PJobsPanels_h

#include "smtk/extension/qt/qtUIManager.h"

#include <QDockWidget>

class pqSMTKWrapper;
class pqServer;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtJobsWidget;
}
} // namespace simulation
} // namespace smtk

class QTableView;

class pqACE3PJobsPanel : public QDockWidget
{
  Q_OBJECT

public:
  pqACE3PJobsPanel(QWidget* parent);
  virtual ~pqACE3PJobsPanel();

Q_SIGNALS:
  void projectUpdated(smtk::project::ProjectPtr);
  void requestNERSCNavigateTo(const QString& dir);

public Q_SLOTS:

protected Q_SLOTS:
  void infoSlot(const QString& msg);

  virtual void sourceRemoved(pqSMTKWrapper* mgr, pqServer* server);

protected:
  smtk::simulation::ace3p::qtJobsWidget* m_jobWidget;
};

#endif // __ACE3PAnalysisPanels_h
