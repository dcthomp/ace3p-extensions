//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PRecentProjectsMenu_h
#define pqACE3PRecentProjectsMenu_h

#include <QObject>

#include <QPointer>

class QAction;
class QMenu;

/** \brief Adds a "Recently Used" submenu to the main Projects menu
  */
class pqACE3PRecentProjectsMenu : public QObject
{
  Q_OBJECT

public:
  /**
  * Assigns the menu that will display the list of files
  */
  pqACE3PRecentProjectsMenu(QMenu* menu, QObject* parent = nullptr);
  ~pqACE3PRecentProjectsMenu() override;

private Q_SLOTS:
  void buildMenu();
  void onOpenProject(QAction*);

private:
  Q_DISABLE_COPY(pqACE3PRecentProjectsMenu);

  QPointer<QMenu> m_menu;
};

#endif // pqACE3PRecentProjectsMenu_h
