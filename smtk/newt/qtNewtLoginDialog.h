//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtNewtLoginDialog.h
// .SECTION Description
// .SECTION See Also

#ifndef smtk_newt_qtNewtLoginDialog
#define smtk_newt_qtNewtLoginDialog

#include "smtk/newt/Exports.h"

#include <QDialog>

namespace Ui
{
class qtNewtLoginDialog;
}

namespace newt
{

class SMTKNEWT_EXPORT qtNewtLoginDialog : public QDialog
{
  Q_OBJECT

public:
  qtNewtLoginDialog(QWidget* parentObject = nullptr);
  ~qtNewtLoginDialog();

  void setErrorMessage(const QString& message);

public Q_SLOTS:
  void accept();
  void reject();

Q_SIGNALS:
  void entered(const QString& username, const QString& password);
  void canceled();

private:
  Ui::qtNewtLoginDialog* ui;
};

} // namespace newt

#endif
