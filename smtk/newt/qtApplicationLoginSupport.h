//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtNewtFileBrowserDialog.h
// .SECTION Description
// .SECTION See Also

#ifndef newt_qtApplicationLoginSupport
#define newt_qtApplicationLoginSupport

#include "smtk/newt/Exports.h"

#include <QObject>

#include "smtk/newt/qtNewtInterface.h"

class QWidget;

/** \brief Provides support for logging into NEWT for applications.
 *
 */
namespace newt
{

class SMTKNEWT_EXPORT qtApplicationLoginSupport : public QObject
{
  Q_OBJECT

public:
  qtApplicationLoginSupport(QWidget* parentWidget = nullptr);
  ~qtApplicationLoginSupport() = default;

  // Returns true if a suitable OpenSSL is found.
  bool checkSSL() const;

Q_SIGNALS:
  // Notifies application of login result
  void loginComplete(bool ok);

public Q_SLOTS:
  // Applications need to connect qtNewtLoginDialog::entered signal to this slot
  void onCredentials(const QString& username, const QString& password);

protected Q_SLOTS:
  void onError(const QString& message);

protected:
  newt::qtNewtInterface* m_newt;
};
} // namespace newt
#endif
