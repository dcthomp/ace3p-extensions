//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME cumulusproxy.h
// .SECTION Description
// .SECTION See Also

#ifndef smtk_cumulus_cumulusproxy_h
#define smtk_cumulus_cumulusproxy_h

#include "smtk/cumulus/jobspanel/Exports.h"
#include "smtk/cumulus/jobspanel/job.h"

#include <QList>
#include <QObject>
#include <QSharedPointer>

class QNetworkCookieJar;
class QNetworkReply;
class QSslError;
class QNetworkAccessManager;

// Singleton class for connecting to Girder/Cumulus server
// to access job information.

namespace cumulus
{

class SMTKCUMULUS_EXPORT CumulusProxy : public QObject
{
  Q_OBJECT

public:
  static QSharedPointer<CumulusProxy> instance();
  ~CumulusProxy() = default;

  void girderUrl(const QString& url);
  bool isAuthenticated();
  bool isGirderRunning(int timeoutSec = 5);

  QNetworkReply* requestJob(const QString& jobId);

public Q_SLOTS:
  void authenticateGirder(const QString& newtSessionId);
  void authenticateNewt(const QString& username, const QString& password);
  void fetchJobs();
  void fetchJob(const QString& id);
  void deleteJob(Job job);
  void patchJobs(QList<Job> jobs);
  void terminateJob(Job job);
  void downloadJob(const QString& downloadDirectory, Job job);

Q_SIGNALS:
  void jobsUpdated(QList<Job> jobs);
  void newtAuthenticationError(const QString& msg);
  void authenticationFinished();
  void error(const QString& msg, QNetworkReply* networkReply = NULL);
  void jobUpdated(cumulus::Job job);
  void jobDeleted(cumulus::Job job);
  void jobTerminated(cumulus::Job job);
  void jobDownloaded(cumulus::Job job, const QString& path);
  void info(const QString& msg);
  void newtSessionId(const QString& sessionId, int lifetime);

private Q_SLOTS:
  void authenticationNewtFinished();
  void authenticationGirderFinished();
  void fetchJobsFinished();
  void fetchJobFinished();
  void deleteJobFinished();
  void terminateJobFinished();
  void sslErrors(const QList<QSslError>& errors);
  void downloadJobFinished();

private:
  CumulusProxy();

  QString m_girderUrl;
  QString m_newtSessionId;
  QString m_girderToken;
  QNetworkAccessManager* m_networkManager;

  Q_DISABLE_COPY(CumulusProxy);
};

} // namespace cumulus

#endif
