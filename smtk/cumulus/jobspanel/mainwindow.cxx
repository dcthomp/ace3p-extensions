//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "mainwindow.h"
#include "job.h"
#include "ui_mainwindow.h"

#include <QAction>
#include <QGuiApplication>
#include <QMessageBox>
#include <QRect>
#include <QScreen>
#include <QStatusBar>
#include <QString>
#include <QTextStream>

#define TEST_CUSTOM_MENU 1

namespace cumulus
{

MainWindow::MainWindow()
  : m_ui(new Ui::MainWindow)
{
  m_ui->setupUi(this);

  // Center the window on the prmary screen
  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();
  int x = (screenGeometry.width() - this->width()) / 2;
  int y = (screenGeometry.height() - this->height()) / 2;
  this->move(x, y);

  this->createMainMenu();

  connect(m_ui->cumulusWidget, SIGNAL(info(QString)), this, SLOT(displayInfo(QString)));

#ifdef TEST_CUSTOM_MENU
  QAction* action = new QAction("Show Download Folder", this);
  m_ui->cumulusWidget->addContextMenuAction("downloaded", action);
  QObject::connect(action, &QAction::triggered, this, &MainWindow::test);
#endif
}

MainWindow::~MainWindow()
{
  delete m_ui;
}

void MainWindow::authenticateGirder(const QString& newtSessionId)
{
  m_ui->cumulusWidget->authenticateGirder(newtSessionId);
}

void MainWindow::girderUrl(const QString& url)
{
  m_ui->cumulusWidget->girderUrl(url);
}

void MainWindow::createMainMenu()
{
  connect(m_ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void MainWindow::closeEvent(QCloseEvent* theEvent)
{
  (void)theEvent;
  qApp->quit();
}

void MainWindow::displayInfo(const QString& msg)
{
  this->statusBar()->showMessage(msg);
}

void MainWindow::test()
{
  QAction* action = qobject_cast<QAction*>(sender());
  if (!action)
  {
    QMessageBox::critical(this, "Test", "Not connected to QAction?");
    return;
  }

  Job job = action->data().value<Job>();
  QString message;
  QTextStream qs(&message);
  qs << "The slot named test() was called for job " << job.id() << ".\n\n"
     << "Download folder: " << job.downloadFolder();
  QMessageBox::information(this, "Test", message);
}

} // namespace cumulus
