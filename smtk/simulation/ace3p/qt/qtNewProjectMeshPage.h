//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_qt_qtNewProjectMeshPage_h
#define smtk_simulation_ace3p_qt_qtNewProjectMeshPage_h

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QDir>
#include <QWizardPage>

namespace Ui
{
class qtNewProjectMeshPage;
}

class QFileDialog;

/// \brief Wizard page for setting new project's model (Genesis) file.

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3PQTEXT_EXPORT qtNewProjectMeshPage : public QWizardPage
{
  Q_OBJECT
  using Superclass = QWizardPage;

public:
  qtNewProjectMeshPage(QWidget* parent = nullptr);
  ~qtNewProjectMeshPage() = default;

  /** \brief Read accessor for the mesh file directory. */
  QString meshFileDirectory() const;
  /** \brief Write accessor for the mesh file directory. */
  void setMeshFileDirectory(const QString& path);

  /** \brief Validates UI form data prior to using/storing those data. */
  bool validatePage() override;

protected Q_SLOTS:
  void browseLocation();

private:
  Ui::qtNewProjectMeshPage* ui;
  QFileDialog* m_fileDialog = nullptr;
  QDir m_meshFileDirectory;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
