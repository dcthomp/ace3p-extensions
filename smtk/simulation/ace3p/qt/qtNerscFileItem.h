//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtNerscFileItem.h Custom smtk item for NERSC filesystem paths.
// .SECTION Description
// .SECTION See Also

#ifndef smtk_simulation_ace3p_qt_qtNerscFileItem
#define smtk_simulation_ace3p_qt_qtNerscFileItem

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/extension/qt/qtAttributeItemInfo.h"
#include "smtk/extension/qt/qtItem.h"

#include <QString>

#include <string>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

/** \brief Custom ItemView for selecting files on NERSC file system
 *
 * Adds 2 buttons to StringItem
 *  Job button (with up arrow) for selecting job results in current project
 *  Browse button for opening NERSC file browser
 * For reasons of backward compatibility, both buttons are displayed by default.
 *
 * ItemView options (xml attributes):
 *   JobButton="emag" for EM results (OMega3P, S3P, T3P) (default)
 *   JobButton="deformed" for DeformedVacuumMesh.ncdf
 *
 *   Upstream="true" to show jobs from previous project stages (default)
 *   Upstream="false" to show jobs from same project stage
 *
 *   DirectoryOnly="false" to display absolute path on NERSC filesystem (default)
 *   DirectoryOnly="true" to only display results directory name (applies to emag only)
 */
class SMTKACE3PQTEXT_EXPORT qtNerscFileItem : public smtk::extension::qtItem
{
  Q_OBJECT
  using Superclass = smtk::extension::qtItem;

public:
  static smtk::extension::qtItem* createItemWidget(
    const smtk::extension::qtAttributeItemInfo& info);
  /* \brief Constructor */
  qtNerscFileItem(const smtk::extension::qtAttributeItemInfo& info);
  /* \brief Destructor */
  virtual ~qtNerscFileItem();

Q_SIGNALS:

public Q_SLOTS:

protected Q_SLOTS:
  /* \brief Opens file browser dialog */
  void onBrowse();
  /* \brief Opens dialog for selecting NERSC file system path from job */
  void onGetNerscPath(const std::string& mode);
  /* \brief Updates UI and StringItem with path sent from external dialog */
  void onDialogApply(const QString& path);
  /* \brief Updates StringItem from user input in QLineEdit */
  void onEditingFinished();
  /* \brief handles checkbox state change */
  void onCheckBoxChanged(int state);

protected:
  void clearChildWidgets();
  void createWidget();
  void setBackground();
  void updateUI();

  /* \brief Adds job button to UI */
  void addJobButton();

private:
  class Internal;
  Internal* m_internal;
};
} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
