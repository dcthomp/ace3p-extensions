//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtMenuSeparator
#define smtk_simulation_ace3p_qt_qtMenuSeparator

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QIcon>
#include <QString>
#include <QWidgetAction>

/** \brief A QWidgetAction that servers as a menu separator with text and icon.
 *
 * Although there is a QMenu::insertSeparator() method that accepts an icon argument,
 * the actual implementation ignores the icon. Each instance of this class can be
 * inserted into a menu and display both text and icon.
 * */

/* Because this class might be more suitable in a different, more generic library,
 * no namespace is applied.
 */

class SMTKACE3PQTEXT_EXPORT qtMenuSeparator : public QWidgetAction
{
  Q_OBJECT

public:
  qtMenuSeparator(const QString& text, const QIcon& icon, QObject* parent = nullptr);
  ~qtMenuSeparator() = default;

protected:
  QWidget* createWidget(QWidget* parent) override;

  QString m_text;
  QIcon m_icon;
};

#endif
