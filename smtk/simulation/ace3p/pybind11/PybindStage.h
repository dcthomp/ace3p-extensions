//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_ace3p_Stage_h
#define pybind_smtk_simulation_ace3p_Stage_h

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "smtk/simulation/ace3p/Stage.h"

#include "smtk/simulation/ace3p/JobsManifest.h"

#include "smtk/attribute/Resource.h"
#include "smtk/model/Resource.h"

namespace py = pybind11;

inline PySharedPtrClass<smtk::simulation::ace3p::Stage> pybind11_init_smtk_simulation_ace3p_Stage(
  py::module& m)
{
  PySharedPtrClass<smtk::simulation::ace3p::Stage> instance(m, "Stage");
  instance.def(py::init<>())
    .def(py::init<
         std::shared_ptr<smtk::attribute::Resource>,
         std::shared_ptr<smtk::model::Resource>,
         std::shared_ptr<smtk::simulation::ace3p::JobsManifest>,
         std::string>())
    .def("attributeResource", &smtk::simulation::ace3p::Stage::attributeResource)
    .def("modelResource", &smtk::simulation::ace3p::Stage::modelResource)
    .def("description", &smtk::simulation::ace3p::Stage::description)
    .def("jobIds", &smtk::simulation::ace3p::Stage::jobIds)
    .def("insertJob", &smtk::simulation::ace3p::Stage::insertJob);
  return instance;
}

#endif
