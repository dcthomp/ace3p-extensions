//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_ace3p_JobsManifest_h
#define pybind_smtk_simulation_ace3p_JobsManifest_h

#include <pybind11/pybind11.h>

#include "smtk/simulation/ace3p/JobsManifest.h"

#include "nlohmann/json.hpp"

namespace py = pybind11;

inline PySharedPtrClass<smtk::simulation::ace3p::JobsManifest>
pybind11_init_smtk_simulation_ace3p_JobsManifest(py::module& m)
{
  PySharedPtrClass<smtk::simulation::ace3p::JobsManifest> instance(m, "JobsManifest");
  instance
    .def(py::init<>())
    // .def("getField", &smtk::simulation::ace3p::JobsManifest::getField)
    .def(
      "addJobRecord",
      [](smtk::simulation::ace3p::JobsManifest& manifest, const std::string& job) {
        return manifest.addJobRecord(job);
      })
    .def("setField", &smtk::simulation::ace3p::JobsManifest::setField)
    .def("write", &smtk::simulation::ace3p::JobsManifest::write)
    .def("read", &smtk::simulation::ace3p::JobsManifest::read)
    .def("size", &smtk::simulation::ace3p::JobsManifest::size)
    // .def("getData", &smtk::simulation::ace3p::JobsManifest::getData)
    ;
  return instance;
}

#endif
