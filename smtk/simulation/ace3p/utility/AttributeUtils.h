//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_AttributeUtils_h
#define smtk_simulation_ace3p_AttributeUtils_h

#include "smtk/simulation/ace3p/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/view/Configuration.h"

#include <string>

// Utility functions for attribute resources

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class SMTKACE3P_EXPORT AttributeUtils
{
public:
  AttributeUtils() = default;
  ~AttributeUtils() = default;

  // Create all instanced attributes based on accessibility from top-level view
  void createInstancedAtts(smtk::attribute::ResourcePtr attResource);

  // Return analysis attribute, creating one if needed
  smtk::attribute::AttributePtr getAnalysisAtt(
    smtk::attribute::ResourcePtr attResource,
    smtk::view::ConfigurationPtr view = nullptr);

protected:
  smtk::attribute::AttributePtr createAttribute(
    smtk::attribute::ResourcePtr attResource,
    const std::string& attType,
    const std::string& attName);

  void recursiveCreateInstancedAtts(
    smtk::attribute::ResourcePtr attResource,
    smtk::view::Configuration::Component& viewComp);
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
