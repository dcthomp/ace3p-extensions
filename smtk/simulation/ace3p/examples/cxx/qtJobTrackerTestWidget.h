//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtJobTrackerTestWidget
#define smtk_simulation_ace3p_qt_qtJobTrackerTestWidget

#include <QWidget>

#include "smtk/PublicPointerDefs.h"

namespace Ui
{
class qtJobTrackerTestWidget;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtCumulusJobTracker;
class qtNewtJobTracker;

/* \brief Widget for standalone testing of qtCumulusJobTracker
 *
 * For internal testing.
 */
class qtJobTrackerTestWidget : public QWidget
{
  Q_OBJECT

public:
  qtJobTrackerTestWidget(QWidget* parentWidget = nullptr);
  ~qtJobTrackerTestWidget() = default;

public Q_SLOTS:

protected Q_SLOTS:
  void onJobStatus(const QString& /*cumulusJobId*/, const QString& status, const QString& jobId);
  void onNewtLoginComplete(const QString& userName);
  void onLoginTrigger();
  void onLoadJobsTrigger();
  void onPollContinuousToggled(bool checked);
  void onPollOnceTrigger();
  void onSubmitJobTrigger();

protected:
#ifdef USE_NEWT_TRACKER
  qtNewtJobTracker* m_jobTracker;
#else
  qtCumulusJobTracker* m_jobTracker;
#endif

private:
  Ui::qtJobTrackerTestWidget* ui;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
