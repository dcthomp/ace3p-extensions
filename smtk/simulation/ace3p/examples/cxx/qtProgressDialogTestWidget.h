//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_examples_cxx_qtProgressDialogTestWidget
#define smtk_simulation_ace3p_examples_cxx_qtProgressDialogTestWidget

#include <QMainWindow>

class QTimer;
class qtProgressDialog;

QT_BEGIN_NAMESPACE
namespace Ui
{
class qtProgressDialogTestWidget;
}
QT_END_NAMESPACE

namespace smtk
{
namespace simulation
{
namespace ace3p
{

/* \brief Widget for standalone testing of qtProgressDialogTestWidget
 *
 * For internal testing.
 */
class qtProgressDialogTestWidget : public QMainWindow
{
  Q_OBJECT

public Q_SLOTS:
  void runCancelled();

public:
  qtProgressDialogTestWidget(QWidget* parent = nullptr);
  ~qtProgressDialogTestWidget();

private Q_SLOTS:
  void on_pushButton_Start_clicked();
  void on_pushButton_MessageDialog_clicked();
  void updateProgress();

private:
  Ui::qtProgressDialogTestWidget* ui;

  QTimer* m_simulatedProgressTimer = nullptr;
  qtProgressDialog* m_progressDialog = nullptr;

  int m_messageNumberTEMP;

  int m_progress;
  int m_minProgress;
  int m_maxProgress;
  bool m_bIndeterminate;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif // smtk_simulation_ace3p_examples_cxx_qtProgressDialogTestWidget
