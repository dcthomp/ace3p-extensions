//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/examples/cxx/qtProgressDialogTestWidget.h"

#include "smtk/simulation/ace3p/qt/qtMessageDialog.h"
#include "smtk/simulation/ace3p/qt/qtProgressDialog.h"

#include "ui_qtProgressDialogTestWidget.h"

#include <QTimer>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtProgressDialogTestWidget::qtProgressDialogTestWidget(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::qtProgressDialogTestWidget)
{
  this->ui->setupUi(this);

  m_simulatedProgressTimer = new QTimer(this);
  connect(m_simulatedProgressTimer, SIGNAL(timeout()), this, SLOT(updateProgress()));

  m_progressDialog = nullptr;
}

qtProgressDialogTestWidget::~qtProgressDialogTestWidget()
{
  delete this->ui;
}

void qtProgressDialogTestWidget::on_pushButton_Start_clicked()
{
  if (m_progressDialog && this->ui->comboBox_ObjectReuse->currentIndex() == 0)
  {
    m_progressDialog->deleteLater();
    m_progressDialog = nullptr;
  }

  m_minProgress = this->ui->spinBox_MinProgress->value();
  m_maxProgress = this->ui->spinBox_MaxProgress->value();
  m_progress = m_minProgress;
  if (m_minProgress == 0 && m_minProgress == m_maxProgress)
  {
    m_bIndeterminate = true;
  }
  else
  {
    m_bIndeterminate = false;
  }

  m_messageNumberTEMP = 0;

  if (!m_progressDialog)
  {
    m_progressDialog = new qtProgressDialog(this, m_minProgress, m_maxProgress, "Demo Progress");
    m_progressDialog->setLabelText("Progress");
  }
  m_progressDialog->setValue(m_minProgress);
  m_progressDialog->setAutoClose(this->ui->checkBox_AutoClose->isChecked());
  m_progressDialog->setMinDuration(this->ui->spinBox_MinDuration->value());
  m_progressDialog->setMessageBoxVisible(
    this->ui->checkBox_MessagesVisible->isChecked(),
    this->ui->checkBox_MessageFiltersVisible->isChecked());
  m_progressDialog->setMessageBoxWordWrap(this->ui->checkBox_WordWrap->isChecked());
  m_progressDialog->setCancelButtonVisible(this->ui->checkBox_CancelButtonVisible->isChecked());
  m_progressDialog->setShowMessageIcons(this->ui->checkBox_InlineIcons->isChecked());
  if (this->ui->checkBox_CancelButtonVisible->isChecked())
  {
    connect(m_progressDialog, SIGNAL(cancelClicked()), this, SLOT(runCancelled()));
  }
  if (this->ui->checkBox_AutoClose->isChecked())
  {
    m_progressDialog->setAutoCloseDelay(this->ui->spinBox_CloseDelay->value());
  }

  m_progressDialog->show();

  m_progressDialog->setProgressText("Long message to allow testing of word wrap: "
                                    "fffffffffffffffffffffffaaaaaa asdddddddd "
                                    "saddddddddd asdddddddddd asddddddd");

  m_simulatedProgressTimer->start(20);
  if (m_bIndeterminate)
  {
    m_simulatedProgressTimer->start(100);
  }
}

void qtProgressDialogTestWidget::runCancelled()
{
  this->ui->textBrowser->append("Run cancelled");
}

void qtProgressDialogTestWidget::updateProgress()
{
  m_progress++;
  m_progressDialog->setValue(m_progress);

  if (m_progress % 10 == 0)
  {
    m_messageNumberTEMP++;
    if (m_progress % 20 == 0)
    {
      m_progressDialog->setProgressText(
        "info message " + QString().number(m_messageNumberTEMP), qtProgressDialog::Info);
    }
    else if (m_progress % 50 == 0)
    {
      m_progressDialog->setProgressText(
        "warning message " + QString().number(m_messageNumberTEMP), qtProgressDialog::Warning);
    }
    else
    {
      m_progressDialog->setProgressText(
        "error message " + QString().number(m_messageNumberTEMP), qtProgressDialog::Error);
    }
  }

  if (m_minProgress != m_maxProgress)
  {
    if (m_progress == m_maxProgress)
    {
      m_simulatedProgressTimer->stop();
    }
  }
}

void qtProgressDialogTestWidget::on_pushButton_MessageDialog_clicked()
{
  qtMessageDialog* pMessageBox = new qtMessageDialog(this);
  pMessageBox->setText("This an example autoclose dialog.");
  pMessageBox->setAutoClose(this->ui->checkBox_AutoClose->isChecked());
  pMessageBox->setAutoCloseDelay(this->ui->spinBox_CloseDelay->value());
  pMessageBox->show();
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
