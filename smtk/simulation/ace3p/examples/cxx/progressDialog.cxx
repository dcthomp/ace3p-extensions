#include "smtk/simulation/ace3p/examples/cxx/qtProgressDialogTestWidget.h"

#include <QApplication>

int main(int argc, char* argv[])
{
  QApplication a(argc, argv);
  smtk::simulation::ace3p::qtProgressDialogTestWidget w;
  w.show();
  return a.exec();
}
