//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtProjectSubmitTestWidget
#define smtk_simulation_ace3p_qt_qtProjectSubmitTestWidget

#include <QWidget>

#include "smtk/PublicPointerDefs.h"

#include "smtk/simulation/ace3p/JobsManifest.h"
#include "smtk/simulation/ace3p/Project.h"

#include <QString>

#include <string>
#include <vector>

namespace Ui
{
class qtProjectSubmitTestWidget;
}

namespace newt
{
class qtDownloadFolderRequester;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtNewtJobSubmitter;
class qtNewtJobTracker;

/* \brief Widget for standalone testing of job-submission
 *
 * For internal testing.
 */
class qtProjectSubmitTestWidget : public QWidget
{
  Q_OBJECT

public:
  qtProjectSubmitTestWidget(
    smtk::project::ManagerPtr projectManager,
    QWidget* parentWidget = nullptr);
  ~qtProjectSubmitTestWidget() = default;

  void setUser(const QString& user);
  void setNewtSessionId(const QString& id);
  void setNerscRepository(const QString& repository);
  void setProjectDirectory(const QString& path);

public Q_SLOTS:

protected Q_SLOTS:
  void onClearMessagesTrigger();
  void onDownloadTrigger();
  void onJobStatus(const QString& /*cumulusJobId*/, const QString& status, const QString& jobId);
  void onNewtLoginComplete(const QString& userName);
  void onLoginTrigger();
  void onPollContinuousToggled(bool checked);
  void onPollOnceTrigger();
  void onProjectTrigger();
  void onSubmitJobTrigger();

protected:
  void updateControls();

  smtk::project::ManagerPtr m_projectManager;
  std::shared_ptr<smtk::simulation::ace3p::Project> m_project;
  QString m_remoteSlurmScript;
  std::vector<std::string> m_exportFiles;
  unsigned int m_downloadCount = 0;
  unsigned int m_uploadCount = 0;

#ifdef USE_NEWT_INTERFACE
  ::newt::qtDownloadFolderRequester* m_downloader;
  qtNewtJobSubmitter* m_jobSubmitter;
  qtNewtJobTracker* m_jobTracker;
#else
#error
#endif

private:
  Ui::qtProjectSubmitTestWidget* ui;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
