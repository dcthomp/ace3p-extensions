//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/ace3p/Registrar.h"

// Plugin includes
#include "smtk/simulation/ace3p/Metadata.h"
#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/operations/Create.h"
#include "smtk/simulation/ace3p/operations/Export.h"
#include "smtk/simulation/ace3p/operations/NewStage.h"
#include "smtk/simulation/ace3p/operations/Read.h"
#include "smtk/simulation/ace3p/operations/RemoveStage.h"
#include "smtk/simulation/ace3p/operations/Write.h"

// SMTK includes
#include "smtk/project/Manager.h"

#include <tuple>

namespace smtk
{
namespace simulation
{
namespace ace3p
{
namespace
{
typedef std::tuple<Create, Export, NewStage, Read, RemoveStage, Write> OperationList;
}

void Registrar::registerTo(const smtk::project::Manager::Ptr& projectManager)
{
  bool projRegistered = projectManager->registerProject<smtk::simulation::ace3p::Project>();
  assert(projRegistered);
  bool opsRegistered = projectManager->registerOperations<OperationList>();
  assert(opsRegistered);
}

void Registrar::unregisterFrom(const smtk::project::Manager::Ptr& projectManager)
{
  // Templated method won't link (undefined reference), so use string instead
  projectManager->unregisterProject(Metadata::PROJECT_TYPENAME);
  projectManager->unregisterOperations<OperationList>();
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
