<?xml version="1.0"?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Simulation</Cat>
    <Cat>Rf-Postprocess</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="ace3p-export" Label="Export to ACE3P" Version="2">
      <ItemDefinitions>
        <Double Name="Pi"><DefaultValue>3.14159</DefaultValue></Double>
        <String Name="NERSCDirectory" Label="NERSC Directory"></String>
        <!-- <Int Name="Answer"><DefaultValue>42</DefaultValue></Int>
        <String Name="xyzzy"></String> -->
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true" FilterByCategory="false" FilterByAdvanceLevel="true">
      <InstancedAttributes>
        <Att Name="Options" Type="ace3p-export">
          <ItemViews>
            <View Item="NERSCDirectory" Type="NERSCDirectory" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>

  </SMTK_AttributeResource>
