<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Track3P</Cat>
    <Cat>Track3P-DarkCurrent</Cat>
    <Cat>Track3P-Multipacting</Cat>
  </Categories>
  <Definitions>
    <!-- Particle Definitions-->
    <AttDef Type="ParticleTrajectories" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="ParticleFile" Label="Particle File" Version="0">
          <BriefDescription>defines the particle file names</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>partpath</DefaultValue>
        </String>
        <Int Name="Start" Label="Start time step" Version="0">
          <BriefDescription>Start time step for writing particles</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="Stop" Label="Stop time step" Version="0">
          <BriefDescription>End time step for writing particles</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>1000000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Int Name="Skip" Label="Skip time step" Version="0">
          <BriefDescription>Skip time step for writing particles. Default is 1 for no skip</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SingleParticleTrajectory" BaseType="" Version="0">
      <ItemDefinitions>
        <Void Name="SingleParticleTrajectory" Label="Single Particle Trajectory" Version="0" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>specifies if all single particle trajectories will be written in separate files</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DetailedDescription>
            When enabled, each single particle trajectory will be written
            in a file named "particleID". When not enabled, no single particle
            trajectory will be output.
          </DetailedDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <!-- Emitter definitions-->
    <AttDef Type="Emitter" BaseType="" Version="0">
      <AssociationsDef Name="EmitterAssociations" NumberOfRequiredValues="1" Extensible="true">
        <Accepts>
          <Resource Name="smtk::model::Resource" Filter="face"></Resource>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Int Name="Type" Label="Type" Version="0">
          <BriefDescription>how the particles are emitted</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Int Name="N" Label="Min electrons (N)" Version="0">
              <BriefDescription>Minimum number of electrons in an emitted macro particle</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>1000</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">1</Min>
              </RangeInfo>
            </Int>
            <Double Name="WorkFunction" Label="Work Function" Version="0" Units="eV">
              <BriefDescription>Work function for field emission</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>4.2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Beta" Label="Beta" Version="0">
              <BriefDescription>Beta value for field emission</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>120.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="2: Electronic Surface">2</Value>
            <Value Enum="5: Solid Electronic Surface">5</Value>
            <Structure>
              <Value Enum="7: Field Emission">7</Value>
              <Items>
                <Item>N</Item>
                <Item>WorkFunction</Item>
                <Item>Beta</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </Int>
        <Int Name="Start" Label="Start time in RF cycle (t0)" Version="0">
          <BriefDescription>Start time when particles are emitted</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="Stop" Label="End time in RF cycle (t1)" Version="0">
          <BriefDescription>End time when particles are emitted</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Int Name="Skip" Label="Skip Time Step" Version="0">
          <BriefDescription>emission skip time step. Default is 1 for no skip</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="BoundsMin" Label="Region Bounds Min" Version="0" NumberOfRequiredValues="3" Units="m">
          <BriefDescription>Minimum coordinates of bounding box for particle emission</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
        <Double Name="BoundsMax" Label="Region Bounds Max" Version="0" NumberOfRequiredValues="3" Units="m">
          <BriefDescription>Maximum coordinates of bounding box for particle emission</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <DefaultValue>1,1,1</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <!-- Field Definitions-->
    <AttDef Type="FieldScales" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="Type" Label="Type" Version="0">
          <BriefDescription>the parameters for the field levels where track3p simulations are carried out</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="Beta" Label="Beta" Units="V/C" Version="0">
              <BriefDescription>Beta parameter for field calculation</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="StartPoint" Label="Start Point" Version="0" NumberOfRequiredValues="3" Units="m">
              <BriefDescription>The coordinates of the start point of the path</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>0,0,0</DefaultValue>
            </Double>
            <Double Name="EndPoint" Label="End Point" Version="0" NumberOfRequiredValues="3" Units="m">
              <BriefDescription>The coordinates of the end point of the path</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>0,0,1</DefaultValue>
            </Double>
            <Int Name="N" Label="Number of inner points (N)" Version="0">
              <BriefDescription>Number of inner points in the gradient integral line</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>300</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">1</Min>
              </RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Field Gradient - V/m">FieldGradient</Value>
              <Items>
                <Item>Beta</Item>
                <Item>StartPoint</Item>
                <Item>EndPoint</Item>
                <Item>N</Item>
              </Items>
            </Structure>
            <Value Enum="InputPortPower - W">InputPortPower</Value>
            <Value Enum="StorEnergy - J"> StorEnergy</Value>
          </DiscreteInfo>
        </String>
        <Int Name="ScanToken" Label="Scan Token" Version="0">
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="Minimum" Label="Minimum" Version="0">
              <BriefDescription>The minimum scanned field level</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>23.e6</DefaultValue>
            </Double>
            <Double Name="Maximum" Label="Maximum" Version="0">
              <BriefDescription>The maximum scanned field level</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>25.e6</DefaultValue>
            </Double>
            <Double Name="Interval" Label="Interval" Version="0">
              <BriefDescription>The interval for scanned field level</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>1.0e6</DefaultValue>
            </Double>
            <Double Name="Scale" Label="Scale" Version="0">
              <BriefDescription>The single field level</BriefDescription>
              <Categories>
                <Cat>Track3P</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Structure>
              <Value Enum="0: Single Field Level">0</Value>
              <Items>
                <Item>Scale</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="1: Scan Field Level">1</Value>
              <Items>
                <Item>Minimum</Item>
                <Item>Maximum</Item>
                <Item>Interval</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>

    <!-- Domain  Definitions-->
    <AttDef Type="Domain" BaseType="" Version="0">
      <!-- Note uses NERSC Directory in ACE3P.sbt-->
      <Categories>
        <Cat>Track3P</Cat>
      </Categories>
      <ItemDefinitions>
        <Int Name="TotalTime" Label="Total Particle Tracking Time" Version="0" Units="Cycles">
          <BriefDescription>Total simulation time in rf cycles</BriefDescription>
          <DefaultValue>20</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Void Name="OutputImpacts" Label="Write Particles Impact Information" Version="0" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>specifies if particle impact information will be written</BriefDescription>
        </Void>
        <Int Name="FrequencyScanID" Label="Frequency Scan ID">
          <DefaultValue>0</DefaultValue>
        </Int>
        <Int Name="Mode" Label="Mode Number" Version="0">
          <BriefDescription>The mode number calculated from the omega3p or s3p imported into track3p</BriefDescription>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Double Name="dt" Label="Time Step Parameter (dt)" Version="0">
          <BriefDescription>Parameter used for timestep; timestep = dt/frequency/100</BriefDescription>
          <DefaultValue> 1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaxImpacts" Label="Max Impacts" Version="0">
          <BriefDescription>Maximum impact number, after which particles will die</BriefDescription>
          <DefaultValue>50</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="LowEnergy" Label="Low Energy" Version="0" Units="eV">
          <BriefDescription>the minimum impact energy, beyond which no secondary particles are emitted</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="HighEnergy" Label="High Energy" Version="0" Units="eV">
          <BriefDescription>the maximum impact energy, beyond which no secondary particles are emitted</BriefDescription>
          <DefaultValue>5000.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="InitialEnergy" Label="Initial Energy" Version="0" Units="eV">
          <BriefDescription>the initial energy of emitted particles</BriefDescription>
          <DefaultValue>2.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="Bins" Label="Localize Bins" Version="0">
          <BriefDescription>Parameter used for localizing particles</BriefDescription>
          <DefaultValue>64</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Component Name="SolidRegion" Label="Solid Region" Version="0" NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Non-vacuum region; only used for simulations with rf windows</BriefDescription>
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="volume"></Resource>
          </Accepts>
        </Component>
        <Component Name="VacuumRegion" Label="Vacuum Region" Version="0" NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Vacuum region; only used for simulations with rf windows</BriefDescription>
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="volume"></Resource>
          </Accepts>
        </Component>
        <Double Name="ExternalMagneticField" Label="External Magnetic Field" Version="0" NumberOfRequiredValues="3" Optional="true" IsEnabledByDefault="false" Units="T">
          <BriefDescription>Uniform external magnetic field</BriefDescription>
          <ComponentLabels>
            <Label>Bx</Label>
            <Label>By</Label>
            <Label>Bz</Label>
          </ComponentLabels>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
        <Group Name="MagneticFieldMap" Label="Magnetic Field Map" Version="0" Optional="true" IsEnabledByDefault="false">
          <ItemDefinitions>
            <File Name="File" Label="File" Version="0" ShouldExist="true">
              <BriefDescription>The name of the file containing the external magnetic field map</BriefDescription>
            </File>
            <Double Name="Scaling" Label="Field Scaling" Version="0">
              <BriefDescription>The field scale factor to multiply the fields in the original field map</BriefDescription>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Units" Label="XYZ Units" Units="m" Version="0">
              <BriefDescription>The distance scale factor to convert original coordinates in the original field map to meters using in track3p</BriefDescription>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="ZOffset" Label="Z Offset" Version="0" Units="m">
              <BriefDescription>The z-offset of the origin when importing the original field map into track3p simulation model</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>

    <!-- Material Definitions-->
    <AttDef Type="Track3PMaterial" BaseType="" Version="1" Abstract="true" Unique="true">
      <AssociationsDef Name="Track3PMaterialAssociations" Version="0" NumberOfRequiredValues="1" Extensible="true">
        <MembershipMask>volume | face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="Track3PAbsorber" BaseType="Track3PMaterial" Label="Absorber" Version="0">
      <Categories>
        <Cat>Track3P</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="Track3PMaterialMultipactingPrimary" BaseType="Track3PMaterial" Label="Primary" Version="1">
      <Categories>
        <Cat>Track3P-Multipacting</Cat>
      </Categories>
      <ItemDefinitions>
        <Void Name="Secondary" Label="Secondary" Optional="true" IsEnabledByDefault="false" Version="0"></Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Track3PMaterialMultipactingSecondary" BaseType="Track3PMaterial" Label="Secondary" Version="0">
      <Categories>
        <Cat>Track3P-Multipacting</Cat>
      </Categories>
    </AttDef>
    <AttDef Type="Track3PMaterialDarkCurrentPrimary" BaseType="Track3PMaterial" Label="Primary" Version="1">
      <Categories>
        <Cat>Track3P-DarkCurrent</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="Secondary" Label="Secondary" Optional="true" IsEnabledByDefault="false" Version="0">
          <ItemDefinitions>
            <File Name="SEYFilename" Label="SEY Filename" Version="0" ShouldExist="true">
              <BriefDescription>File containing secondary emission data for the selected surface</BriefDescription>
            </File>
            <Int Name="MinimumNumElectrons" Label="Minimum Number of Electrons" Version="0">
              <BriefDescription>Minimum number of electrons in each secondary macro particle</BriefDescription>
              <DefaultValue>1000</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">1</Min>
              </RangeInfo>
            </Int>
            <Int Name="ElasticThreshold" Label="Elastic Threshold" Version="0">
              <BriefDescription>Minimum number of electrons in each secondary reflected-type macro particle</BriefDescription>
              <DefaultValue>1000</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">1</Min>
              </RangeInfo>
            </Int>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Track3PMaterialDarkCurrentSecondary" BaseType="Track3PMaterial" Label="Secondary" Version="1">
      <Categories>
        <Cat>Track3P-DarkCurrent</Cat>
      </Categories>
      <ItemDefinitions>
        <File Name="SEYFilename" Label="SEY Filename" Version="0" ShouldExist="true">
          <BriefDescription>File containing secondary emission data for the selected surface</BriefDescription>
        </File>
        <Int Name="MinimumNumElectrons" Label="Minimum Number of Electrons" Version="0">
          <BriefDescription>Minimum number of electrons in each secondary macro particle</BriefDescription>
          <DefaultValue>1000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Int Name="ElasticThreshold" Label="Elastic Threshold" Version="0">
          <BriefDescription>Minimum number of electrons in each secondary reflected-type macro particle</BriefDescription>
          <DefaultValue>1000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>

    <!-- Postprocess Definitions-->
    <AttDef Type="Postprocess" BaseType="" Version="0">
      <Categories>
        <Cat>Track3P</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="Toggle" Label="Toggle" Version="0" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Turn on/off the postprocessing procedure</BriefDescription>
          <ItemDefinitions>
            <Group Name="ResonantParticles" Lable="Resonant Particles" Version="0" Optional="true" IsEnabledByDefault="true">
              <ItemDefinitions>
                <Int Name="InitialImpacts" Label="Initial Impacts" Version="0">
                  <BriefDescription>Particles with impact number greater than specified threshold are considered</BriefDescription>
                  <DefaultValue>4</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0</Min>
                  </RangeInfo>
                </Int>
                <Double Name="EnergyRange" Label="Energy Range" Version="0" NumberOfRequiredValues="2">
                  <BriefDescription>The energy range to consider if a particle exhibits a resonant trajectory</BriefDescription>
                  <ComponentLabels>
                    <Label>From (E1)</Label>
                    <Label>To (E2)</Label>
                  </ComponentLabels>
                  <DefaultValue>24,10000</DefaultValue>
                </Double>
                <Double Name="PhaseTolerance" Label="Phase Tolerance" Version="0">
                  <BriefDescription>Phase tolerance to determine resonant particles</BriefDescription>
                  <DefaultValue>0.01</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0</Min>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
            <Group Name="EnhancementCounter" Label="Enhancement Counter" Version="0" Optional="true" IsEnabledByDefault="true">
              <ItemDefinitions>
                <Group Name="SEYSurface1" Label="SEY Surface 1" Verion="0" Optional="true" IsEnabledByDefault="false">
                  <ItemDefinitions>
                    <Component Name="BoundarySurface" Label="Boundary Surface" NumberOfRequiredValues="1" Version="0">
                      <BriefDescription>The boundary surface used for calculating the enhancement counter</BriefDescription>
                      <Accepts>
                        <Resource Name="smtk::model::Resource" Filter="face"></Resource>
                      </Accepts>
                    </Component>
                    <File Name="SEYFilename" Label="SEY Filename" Version="0" ShouldExist="true">
                      <BriefDescription>File containing secondary emission data for the selected surface</BriefDescription>
                    </File>
                  </ItemDefinitions>
                </Group>
                <Group Name="SEYSurface2" Label="SEY Surface 2" Verion="0" Optional="true" IsEnabledByDefault="false">
                  <ItemDefinitions>
                    <Component Name="BoundarySurface" Label="Boundary Surface" NumberOfRequiredValues="1" Version="0">
                      <BriefDescription>The boundary surface used for calculating the enhancement counter</BriefDescription>
                      <Accepts>
                        <Resource Name="smtk::model::Resource" Filter="face"></Resource>
                      </Accepts>
                    </Component>
                    <File Name="SEYFilename" Label="SEY Filename" Version="0" ShouldExist="true">
                      <BriefDescription>File containing secondary emission data for the selected surface</BriefDescription>
                    </File>
                  </ItemDefinitions>
                </Group>
                <Component Name="WindowVolume" Label="Window Volume" NumberOfRequiredValues="1" Version="0" Optional="true" IsEnabledByDefault="false">
                  <BriefDescription>Window volume</BriefDescription>
                  <Accepts>
                    <Resource Name="smtk::model::Resource" Filter="face"></Resource>
                  </Accepts>
                </Component>
                <Double Name="MinimumEC" Label="Minimum Enhancement Counter" Version="0">
                  <BriefDescription>Minimum enhancement counter, less than which no data will be written to file</BriefDescription>
                  <DefaultValue>10.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
            <Group Name="FaradayCup" Label="Faraday Cup" Optional="true" IsEnabledByDefault="false">
              <ItemDefinitions>
                <Group Name="BoundarySurfaceGroup" Label="Boundary Surfaces" Extensible="true" NumberOfRequiredValues="1">
                  <ItemDefinitions>
                    <Component Name="BoundarySurface" Label=" " NumberOfRequiredValues="1" Version="0">
                      <Accepts>
                        <Resource Name="smtk::model::Resource" Filter="face"></Resource>
                      </Accepts>
                    </Component>
                  </ItemDefinitions>
                </Group>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!-- SEY Curve Definition (expression)-->
  <!--   AttDef(Type="SimpleExpression" BaseType="" Abstract="true" Version="0")-->
  <!--   AttDef(Type="SEYCurve" Label="SEY Curve" BaseType="SimpleExpression" Version="0")-->
  <!--     ItemDefinitions-->
  <!--       Group(Name="ValuePairs" Label="Value Pairs" NumberOfRequiredGroups="1")-->
  <!--         ItemDefinitions-->
  <!--           Double(Name="Einit" Label="Impact Energy" Units="eV" NumberOfRequiredValues="0" Extensible="true")-->
  <!--             RangeInfo #[Min(Inclusive="true")] 0.0-->
  <!--           Double(Name="Sigma0" Label="SEY Yield Coefficient" Version="0" NumberOfRequiredValues="0" Extensible="true")-->
  <!--             RangeInfo #[Min(Inclusive="true")] 0.0-->
  <!--       String(Name="Sim1DTable" Version="0" NumberOfRequiredValues="1")/-->
  <!-- = '\n'-->
  <Views>
    <!-- View(Type="Group" Title="Track3P" TopLevel=toplevel TabPosition="North"-->
    <!--     FilterByAdvanceLevel="false")-->
    <!--   Views-->
    <!--     View(Title="Domain")/-->
    <!--     View(Title="Field")/-->
    <!--     View(Title="Particles")/-->
    <!--     View(Title="Track3P BC")/-->
    <!--     View(Title="Track3P Material Model")/-->
    <!--     View(Title="Postprocess")/-->
    <!--     View(Title="SEY Curves")/-->
    <!-- = '\n'-->
    <View Type="Instanced" Title="Domain" Label="Analysis">
      <InstancedAttributes>
        <Att Name="Domain" Type="Domain"/>
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Field" Label="Field Scaling">
      <InstancedAttributes>
        <Att Name="FieldScales" Type="FieldScales"/>
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Particles" Style="Tiled" Label="Emitter">
      <Views>
        <View Title="Particles Trajectories"/>
        <View Title="Emitters"/>
      </Views>
    </View>
    <View Type="Instanced" Title="Particles Trajectories">
      <InstancedAttributes>
        <Att Name="ParticleTrajectories" Type="ParticleTrajectories"/>
        <Att Name="SingleParticleTrajectory" Type="SingleParticleTrajectory"/>
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Emitters" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="Emitter"/>
      </AttributeTypes>
    </View>

    <View Type="Attribute" Name="Track3P Material Model" Label="Material" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="Track3PMaterial"></Att>
      </AttributeTypes>
    </View>
    <View Type="ModelEntity" Title="Track3PMultipacting" ModelEntityFilter="rf">
      <AttributeTypes>
        <Att Type="Track3PMaterialMultipactingPrimary"></Att>
        <Att Type="Track3PMaterialMultipactingSecondary"></Att>
        <Att Type="Track3PAbsorber"></Att>
      </AttributeTypes>
    </View>
    <View Type="ModelEntity" Title="Track3PDarkCurrent" ModelEntityFilter="rf">
      <AttributeTypes>
        <Att Type="Track3PMaterialDarkCurrentPrimary"></Att>
        <Att Type="Track3PMaterialDarkCurrentSecondary"></Att>
        <Att Type="Track3PAbsorber"></Att>
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Postprocess">
      <InstancedAttributes>
        <Att Name="Postprocess" Type="Postprocess"/>
      </InstancedAttributes>
    </View>
    <View Type="SimpleExpression" Title="SEY Curves">
      <Att Type="SEYCurve"></Att>
    </View>
  </Views>
</SMTK_AttributeResource>
