<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Categories><Cat>T3P</Cat></Categories>

  <Definitions>
    <AttDef Type="BeamLoading" Label="Beam Loading">
      <Categories><Cat>T3P</Cat></Categories>
      <ItemDefinitions>
        <String Name="Bunch Type" Version="0">
          <BriefDescription>Indicates the type of the rigid beam that traverses the domain</BriefDescription>

          <ChildrenDefinitions>
            <Double Name="Sigma" Label="RMS Bunch Length" Units="m">
              <BriefDescription>(Sigma) RMS Bunch Length for the symmetric Gaussian Bunch</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Double>
            <Double Name="Sigma1" Label="Front Side RMS Bunch Length" Units="m">
              <BriefDescription>(Sigma1) RMS Length for the front side of the asymmetric Gaussian Bunch</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Double>
            <Double Name="Sigma2" Label="Back Side RMS Bunch Length" Units="m">
              <BriefDescription>(Sigma2) RMS Length for the back side of the asymmetric Gaussian Bunch</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Double>
            <Int Name="numSigmas" Label="Number of Sigmas">
              <BriefDescription>The total length of the Bunch</BriefDescription>
              <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Gaussian">Gaussian</Value>
                <Items>
                  <Item>Sigma</Item>
                  <Item>numSigmas</Item>
                </Items>
            </Structure>
            <Structure>
              <Value Enum="Bi-Gaussian">Gaussian</Value>
                <Items>
                  <Item>Sigma1</Item>
                  <Item>Sigma2</Item>
                  <Item>numSigmas</Item>
                </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="SymmetryFactor" Label="Symmetry Factor">
          <BriefDescription>If the bunch trajectory is located on a symmetry plane, this sets the correct value of Charge </BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo><Min Inclusive="true">1.0</Min></RangeInfo>
        </Double>
        <Double Name="Charge" Label="Charge" Units="C">
          <BriefDescription>Total Charge of the Bunch</BriefDescription>
        </Double>

        <Double Name="StartPoint" Label="Starting Point" NumberOfRequiredValues="3" Units="m">
          <BriefDescription>Location of where the beam enters the domain</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
        <Double Name="Direction" Label="Direction" NumberOfRequiredValues="3">
          <BriefDescription>The direction of the bunch; only Z direction is currently allowed</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,1</DefaultValue>
        </Double>
        <Double Name="StartTime" Label="Start Time" AdvanceLevel="1" Units="s">
          <BriefDescription>Time when the bunch is injected</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
        <Component Name="SourceBoundary" Label="Source Boundary" NumberOfRequiredValues="1">
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="face" />
          </Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
