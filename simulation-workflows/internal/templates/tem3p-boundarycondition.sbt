<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Harmonic</Cat>
    <Cat>TEM3P-Elastic</Cat>
    <Cat>TEM3P-Linear-Thermal</Cat>
    <Cat>TEM3P-Nonlinear-Thermal</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="TEM3PBoundaryCondition" Version="0" BaseType="" Abstract="true" Unique="false">
      <AssociationsDef Name="TEM3PBCAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="TEM3PMechanicalBC" BaseType="TEM3PBoundaryCondition" Abstract="true" Unique="true" Version="0"></AttDef>
    <AttDef Type="TEM3PStructuralNeumann" Label="Normal Loading (Neumann)" BaseType="TEM3PMechanicalBC" Version="0">
      <ItemDefinitions>
        <Double Name="NeumannValue" Label="Normal Loading" Version="0" Units="Pa">
          <BriefDescription>Fixed normal loading into the surface</BriefDescription>
          <Categories>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <!-- Placeholder item for eigenmode (user cannot change value)-->
        <Void Name="PlaceHolder" AdvanceLevel="99">
          <Categories>
            <Cat>TEM3P-Eigen</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PStructuralDirichlet" Label="Displacement (Dirichlet)" BaseType="TEM3PMechanicalBC" Version="0">
      <ItemDefinitions>
        <Double Name="DirichletValue" Label="Displacement (Dirichlet)" Version="0" Units="m">
          <BriefDescription>Fixed displacement</BriefDescription>
          <Categories>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <!-- Placeholder item for eigenmode (user cannot change value)-->
        <Void Name="PlaceHolder" AdvanceLevel="99">
          <Categories>
            <Cat>TEM3P-Eigen</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PStructuralMixed" Label="Mixed" BaseType="TEM3PMechanicalBC" Version="0">
      <ItemDefinitions>
        <String Name="MixedType" Label="Mixed Type" Version="0" NumberOfRequiredValues="3">
          <BriefDescription>Boundary condition type in x, y, z directions</BriefDescription>
          <Categories>
            <Cat>TEM3P-Eigen</Cat>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x</Label>
            <Label>y</Label>
            <Label>z</Label>
          </ComponentLabels>
          <DiscreteInfo>
            <Value Enum="Displacement (Dirichlet)">DIRICHLET</Value>
            <Value Enum="Normal Loading (Neumann)">NEUMANN</Value>
          </DiscreteInfo>
        </String>
        <Double Name="MixedValue" Label="Mixed Value" Version="0" NumberOfRequiredValues="3">
          <BriefDescription>Value of each type in the corresponding direction</BriefDescription>
          <Categories>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x</Label>
            <Label>y</Label>
            <Label>z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PLFDetuning" Label="LF Detuning" BaseType="TEM3PMechanicalBC" Version="0">
      <!-- Note uses NERSC Directory in ACE3P.sbt-->
      <ItemDefinitions>
        <Int Name="WhichMode" Label="Mode Number" Version="0">
          <BriefDescription>The mode number calculated from the omega3p results to be imported into tem3p</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="Omega3PId" Label="Omega3P Id" Version="0">
          <BriefDescription>Sideset id of the interface surface with Omega3P</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <String Name="LFDetuningMethod" Label="Method" Version="0">
          <BriefDescription>Method to be used for field normalization</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="TargetGradient" Label="Target Gradient" Units="V/m" Version="0">
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="TargetVoltage" Label="Target Voltage" Units="V" Version="0">
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="VoltageDirection" Label="Voltage Direction" Units="m" NumberOfRequiredValues="3" Version="0">
              <BriefDescription>Orthonormal Vector</BriefDescription>
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <DefaultValue>0,0,1</DefaultValue>
            </Double>
            <Double Name="TargetPowerinput" Label="Target Power Input" Units="W" Version="0">
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Int Name="SymmetryFactor" Label="Symmetry Factor">
              <BriefDescription>0 - full cell, 1 - half cell, 2 - quarter cell</BriefDescription>
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="0 = Full Cell">0</Value>
                <Value Enum="1 = Half Cell">1</Value>
                <Value Enum="2 = Quarter Cell">2</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="StartPoint" Label="Start Point" NumberOfRequiredValues="3" Units="m" Version="0">
              <BriefDescription>The coordinates of the start point of the path</BriefDescription>
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <ComponentLabels>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0,0,0</DefaultValue>
            </Double>
            <Double Name="EndPoint" Label="End Point" NumberOfRequiredValues="3" Units="m" Version="0">
              <BriefDescription>The coordinates of the end point of the path</BriefDescription>
              <Categories>
                <Cat>TEM3P-Harmonic</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <ComponentLabels>
                <Label>x</Label>
                <Label>y</Label>
                <Label>z</Label>
              </ComponentLabels>
              <DefaultValue>0,0,1</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <!-- (LFDetuningMethod)-->
          <DiscreteInfo>
            <Structure>
              <Value>Gradient</Value>
              <Items>
                <Item>TargetGradient</Item>
                <Item>SymmetryFactor</Item>
                <Item>StartPoint</Item>
                <Item>EndPoint</Item>
              </Items>
            </Structure>
            <Structure>
              <Value>Voltage</Value>
              <Items>
                <Item>TargetVoltage</Item>
                <Item>VoltageDirection</Item>
                <Item>SymmetryFactor</Item>
                <Item>StartPoint</Item>
                <Item>EndPoint</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Power Input">Powerinput</Value>
              <Items>
                <Item>TargetPowerinput</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Harmonic BCs have different labels, so.....-->
    <AttDef Type="TEM3PHarmonicNeumann" Label="Normal Loading Amplitude (Neumann)" BaseType="TEM3PMechanicalBC" Version="0">
      <ItemDefinitions>
        <Double Name="NeumannValue" Label="Normal Loading Amplitude" Version="0" Units="Pa">
          <BriefDescription>Normal loading amplitude</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PHarmonicDirichlet" Label="Displacement Amplitude (Dirichlet)" BaseType="TEM3PMechanicalBC" Version="0">
      <ItemDefinitions>
        <Double Name="DirichletValue" Label="Displacement Amplitude" Version="0" Units="m">
          <BriefDescription>Fixed displacement amplitude</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PHarmonicMixed" Label="Mixed" BaseType="TEM3PMechanicalBC" Version="0">
      <ItemDefinitions>
        <String Name="MixedType" Label="Mixed Type" Version="0" NumberOfRequiredValues="3">
          <BriefDescription>Boundary condition type in x, y, z directions</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x</Label>
            <Label>y</Label>
            <Label>z</Label>
          </ComponentLabels>
          <DiscreteInfo>
            <Value Enum="Displacement (Dirichlet)">DIRICHLET</Value>
            <Value Enum="Normal Loading (Neumann)">NEUMANN</Value>
          </DiscreteInfo>
        </String>
        <Double Name="MixedValue" Label="Mixed Value Amplitude" Version="0" NumberOfRequiredValues="3">
          <BriefDescription>Value of each type in the corresponding direction</BriefDescription>
          <Categories>
            <Cat>TEM3P-Harmonic</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x</Label>
            <Label>y</Label>
            <Label>z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PThermalBC" BaseType="TEM3PBoundaryCondition" Abstract="true" Unique="true" Version="0"></AttDef>
    <AttDef Type="TEM3PThermalNeumann" Label="Heat Flux (Neumann)" BaseType="TEM3PThermalBC" Version="0">
      <ItemDefinitions>
        <Double Name="NeumannValue" Label="Heat Flux" Version="0" Units="W/m^2">
          <BriefDescription>Fixed heat flux into the surface (Neumann)</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PThermalDirichlet" Label="Temperature (Dirichlet)" BaseType="TEM3PThermalBC" Version="0">
      <ItemDefinitions>
        <Double Name="DirichletValue" Label="Temperature" Version="0" Units="K">
          <BriefDescription>Fixed temperature (Dirichlet)</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PThermalRobin" Label="Robin" BaseType="TEM3PThermalBC" Version="0">
      <ItemDefinitions>
        <Double Name="RobinConstantFactor" Label="Robin Constant Factor" Units="W/(m^2K)" Version="0">
          <BriefDescription>Convective coefficient</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="RobinConstantValue" Label="Robin Constant Value" Units="W/(m^2)" Version="0">
          <BriefDescription>Product of convective coefficient and ambient temperature</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PThermalRFHeating" Label="RF Heating" BaseType="TEM3PThermalBC" Version="0">
      <ItemDefinitions>
        <!-- Note uses NERSC Directory in ACE3P.sbt-->
        <Int Name="WhichMode" Label="Mode Number" Version="0">
          <BriefDescription>The mode number calculated from the omega3p or s3p data imported into tem3p</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <String Name="Method" Label="Method" Version="0">
          <BriefDescription>Method for specifying RF heating</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="TargetPowerinput" Label="Target Power Input" Units="W" Version="0">
              <BriefDescription>Total power input to the system</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="Sigma" Label="Electrical Conductivity (Sigma)" Units="S/m" Version="0">
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="true"></Min> 0.0
              </RangeInfo>
            </Double>
            <Double Name="TargetGradient" Label="Target Gradient" Units="V/m" Version="0">
              <BriefDescription>Accelerating gradient</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="DutyFactor" Label="Duty Factor" Version="0">
              <BriefDescription>Fraction of time during operation for average power calculation</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <RangeInfo>
                <Min inclusive="true">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
            <Double Name="StartPoint" Label="Start Point" NumberOfRequiredValues="3" Units="m" Version="0">
              <BriefDescription>The coordinates of the start point for gradient integration</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <DefaultValue>0,0,0</DefaultValue>
            </Double>
            <Double Name="EndPoint" Label="End Point" NumberOfRequiredValues="3" Units="m" Version="0">
              <BriefDescription>The coordinates of the end point for gradient integration</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <DefaultValue>0,0,1</DefaultValue>
            </Double>
            <Double Name="TargetPowerLoss" Label="Target Power Loss" Units="W" Version="0">
              <BriefDescription>Total power loss on metallic surface</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <!-- (RFHeatingMethod)-->
          <DiscreteInfo>
            <Structure>
              <Value Enum="Power Input">Powerinput</Value>
              <Items>
                <Item>TargetPowerinput</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Target Gradient Scaling">Gradient</Value>
              <Items>
                <Item>Sigma</Item>
                <Item>TargetGradient</Item>
                <Item>DutyFactor</Item>
                <Item>StartPoint</Item>
                <Item>EndPoint</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Target Power Loss Scaling">PowerLoss</Value>
              <Items>
                <Item>TargetPowerLoss</Item>
                <Item>Sigma</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Int Name="SymmetryFactor" Label="Symmetry Factor">
          <BriefDescription>1 for full cell, 0 for half cell</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="1 = Full Cell">1</Value>
            <Value Enum="0 = Half Cell">0</Value>
            <Value Enum="2 = Quarter Cell">2</Value>
          </DiscreteInfo>
        </Int>
        <String Name="SurfaceResistance" Label="Surface Resistance">
          <BriefDescription>Nonlinear surface resistance</BriefDescription>
          <Categories>
            <Cat>TEM3P-Nonlinear-Thermal</Cat>
          </Categories>
          <ChildrenDefinitions>
            <File Name="SurfaceResistanceFile" Label="Nonlinear Surface Resistance (file)" NumberOfRequiredValues="1" ShouldExist="true" Version="0">
              <BriefDescription>File that defines the nonlinear surface resistance</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
            </File>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <!-- The values are filenames-->
            <Value Enum="Not Used">None</Value>
            <Value Enum="Copper with RRR10">SRinnerCURRR10</Value>
            <Value Enum="Copper with RRR30">SRinnerCURRR30</Value>
            <Value Enum="Copper with RRR50">SRinnerCURRR50</Value>
            <Value Enum="Copper with RRR100">SRinnerCURRR100</Value>
            <Value Enum="NbTi">SRinnerNbTi</Value>
            <Value Enum="Aluminum">SRinnerAA</Value>
            <Value Enum="Nb">SRinnerNB</Value>
            <Value Enum="Stainless Steel">SRinnerSS</Value>
            <Structure>
              <Value Enum="Custom...">Custom</Value>
              <Items>
                <Item>SurfaceResistanceFile</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Group Name="NonlinearConvectiveSurface" Label="Nonlinear Convective Surface" Optional="true" IsEnabledByDefault="false" Version="0">
          <BriefDescription>(Robin)</BriefDescription>
          <ItemDefinitions>
            <File Name="RobinFactor" Label="Robin Factor (file)" NumberOfRequiredValues="1" ShouldExist="true" Version="0">
              <BriefDescription>File that defines the nonlinear Robin factor, convective coefficient (W/(m^2K))</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
            </File>
            <File Name="RobinValue" Label="Robin Value (file)" NumberOfRequiredValues="1" ShouldExist="true" Version="0">
              <BriefDescription>File that defines the nonlinear Robin value, product of convective coefficient and ambient temperature (W/m^2)</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat>
              </Categories>
            </File>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
