<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>T3P</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Loading" Label="Loading" BaseType="" Abstract="true" Version="0" />
    <AttDef Type="DipoleLoading" Label="Dipole Loading" BaseType="Loading" Version="0">
      <BriefDescription>Excitation of a dipole at a point in the computational volume</BriefDescription>
      <ItemDefinitions>
        <Double Name="Coordinate" Label="Coordinate" Version="0"
          NumberOfRequiredValues="3" Units="m">
          <BriefDescription>The location of the dipole</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
        <Double Name="Direction" Label="Direction" NumberOfRequiredValues="3">
          <BriefDescription>The direction of the dipole</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,1</DefaultValue>
        </Double>
        <Double Name="Tolerance" Label="Tolerance" Version="0">
          <BriefDescription>The tolerance that the dipole Direction aligns with the edge of the element closest to the dipole Coordinate</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
          <DefaultValue>0.001</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Group Name="Excitation" Label="Excitation" Version="0">
          <ItemDefinitions>
            <Double Name="Power" Label="Power" Version="0" Units="W">
              <BriefDescription>Power of pulse</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Max Inclusive="false">0.0</Max>
              </RangeInfo>
            </Double>
            <String Name="Pulse" Label="Pulse Type" Version="0">
              <BriefDescription>Type of excitation</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="Frequency" Label="Frequency" Version="0" Units="Hz">
                  <BriefDescription>Mean frequency for Gaussian; Drive frequency for Monochromatic</BriefDescription>
                  <Categories>
                    <Cat>T3P</Cat>
                  </Categories>
                  <DefaultValue>1.e10</DefaultValue>
                  <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
                </Double>
                <Double Name="Bandwidth" Label="Bandwidth" Version="0" Units="Hz">
                  <BriefDescription>Pulse Bandwidth</BriefDescription>
                  <Categories>
                    <Cat>T3P</Cat>
                  </Categories>
                  <DefaultValue>1.e9</DefaultValue>
                  <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
                </Double>
                <Double Name="CutoffDB" Label="Cutoff" Version="0"> <!-- Units="dB" -->
                  <BriefDescription>Cutoff in dB of the Gaussian from peak value</BriefDescription>
                  <Categories>
                    <Cat>T3P</Cat>
                  </Categories>
                  <DefaultValue>20</DefaultValue>
                  <RangeInfo><Min Inclusive="true">0.0</Min></RangeInfo>
                </Double>
                <Double Name="StartTime" Label="Start Time" Version="0" Units="s">
                  <BriefDescription>(T0) Start time of the pulse</BriefDescription>
                  <Categories>
                    <Cat>T3P</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                </Double>
                <Double Name="Phase" Label="Phase" Version="0" Units="degree">
                  <BriefDescription>Phase for the sinusoidal oscillation at the mean frequency</BriefDescription>
                  <Categories>
                    <Cat>T3P</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">-360.0</Min>
                    <Max Inclusive="true">360.0</Max>
                  </RangeInfo>
                </Double>
                <Int Name="RisePeriods" Label="Rise Periods" Version="0">
                  <BriefDescription>Number of cycles to reach steady state from initial zero value</BriefDescription>
                  <Categories>
                    <Cat>T3P</Cat>
                  </Categories>
                  <DefaultValue>10</DefaultValue>
                  <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
                </Int>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Gaussian">Gaussian</Value>
                  <Items>
                    <Item>Frequency</Item>
                    <Item>Bandwidth</Item>
                    <Item>CutoffDB</Item>
                    <Item>StartTime</Item>
                    <Item>Phase</Item>
                  </Items>
                </Structure>
                <Structure>
                  <Value Enum="Monochromatic">Monochromatic</Value>
                  <Items>
                    <Item>Frequency</Item>
                    <Item>RisePeriods</Item>
                    <Item>StartTime</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="PortModeLoading" Label="Port Mode Loading" BaseType="Loading" Version="0">
      <BriefDescription>Field excitation at a boundary surface</BriefDescription>
      <Categories>
        <Cat>T3P</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="ScalingFactor" Label="ScalingFactor" Version="0">
          <BriefDescription>Factor to scale the drive fields</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
        </Double>
        <Component Name="BoundaryPort" Label="Boundary Port" Version="0"
          NumberOfRequiredValues="1">
          <BriefDescription>The boundary port</BriefDescription>
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="face" />
          </Accepts>
        </Component>
        <Double Name="Origin" Label="Origin" NumberOfRequiredValues="3" Units="m">
          <BriefDescription>Coordinates of the origin of the port boundary</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0,0,0</DefaultValue>
        </Double>
        <Double Name="XDirection" Label="X Direction" NumberOfRequiredValues="3">
          <BriefDescription>The x direction of the port</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>1,0,0</DefaultValue>
        </Double>
        <Double Name="YDirection" Label="Y Direction" NumberOfRequiredValues="3">
          <BriefDescription>The y direction of the port</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0,1,0</DefaultValue>
        </Double>
        <String Name="ESolver" Label="ESolver Type" Version="0">
          <BriefDescription>Solution of the port mode</BriefDescription>
          <ChildrenDefinitions>
            <File Name="ExFile" Label="E Field X Data File" Version="0" ShouldExist="true">
              <BriefDescription>File name containing the x component of the electric field</BriefDescription>
            </File>
            <File Name="EyFile" Label="E Field Y Data File" Version="0" ShouldExist="true">
              <BriefDescription>File name containing the y component of the electric field</BriefDescription>
            </File>
            <File Name="BxFile" Label="B Field X Data File" Version="0" ShouldExist="true">
              <BriefDescription>File name containing the x component of the magnetic field</BriefDescription>
            </File>
            <File Name="ByFile" Label="B Field Y Data File" Version="0" ShouldExist="true">
              <BriefDescription>File name containing the y component of the magnetic field</BriefDescription>
            </File>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Interpolative">Interpolative</Value>
              <Items>
                <Item>ExFile</Item>
                <Item>EyFile</Item>
                <Item>BxFile</Item>
                <Item>ByFile</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  </SMTK_AttributeResource>
