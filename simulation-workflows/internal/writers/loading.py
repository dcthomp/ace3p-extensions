"""
"""
from imp import reload

from . import utils
from . import cardformat
reload(cardformat)
from .cardformat import CardFormat


def write_loading(scope):
    '''Writes Loading sections for T3P input files

    '''
    att_list = scope.sim_atts.findAttributes('Loading')
    if not att_list:
        return
    att_list.sort(key=lambda att: att.name())

    print('Write dipole and port mode excitation')
    for att in att_list:
        scope.output.write('\n')
        scope.output.write('Loading:\n')
        scope.output.write('{\n')

        if att.type() == 'DipoleLoading':
            write_dipole_loading(scope, att)
        elif att.type() == 'PortModeLoading':
            write_portmode_loading(scope, att)
        else:
            msg = 'Unrecognized Loading type %s' % att.type()
            scope.logger.addError(msg)
            raise Exception(msg)

        scope.output.write('}\n')


def write_dipole_loading(scope, att):
    scope.output.write('  Type: DipoleLoading\n')
    indent2 = '  '
    indent4 = '    '
    indent6 = '      '
    CardFormat('Coordinate', 'Coordinate', fmt='%g').write(scope, att, indent2)
    CardFormat('Direction', 'Direction', fmt='%g').write(scope, att, indent2)
    CardFormat('Tolerance', 'Tolerance', fmt='%g').write(scope, att, indent2)
    scope.output.write('  Excitation:\n')
    scope.output.write('  {\n')
    CardFormat('Power', 'Excitation/Power', '%g').write(scope, att, indent4)

    scope.output.write('    Pulse:\n')
    scope.output.write('    {\n')
    CardFormat('Type', 'Excitation/Pulse', '%s').write(scope, att, indent6)
    CardFormat('Frequency', 'Excitation/Pulse/Frequency', '%g').write(scope, att, indent6)

    # Currently need to get pulse type to write its items
    # Ideally, the Attribute::itemAtPath() could be constrained to active children
    # In the meantime....
    pulse_item = att.itemAtPath('Excitation/Pulse')
    if pulse_item.value(0) == 'Gaussian':
        CardFormat('Bandwidth', 'Excitation/Pulse/Bandwidth', '%g').write(scope, att, indent6)
        CardFormat('DB', 'Excitation/Pulse/CutoffDB', '%g').write(scope, att, indent6)
        CardFormat('T0', 'Excitation/Pulse/StartTime', '%g').write(scope, att, indent6)
        CardFormat('Phase', 'Excitation/Pulse/Phase', '%g').write(scope, att, indent6)
    elif pulse_item.value(0) == 'Monochromatic':
        CardFormat('RisePeriods', 'Excitation/Pulse/RisePeriods', '%d').write(scope, att, indent6)
        CardFormat('T0', 'Excitation/Pulse/StartTime', '%g').write(scope, att, indent6)
    else:
        msg = 'Unrecognized Pulse type %s' % pulse_item.value(0)
        scope.logger.addError(msg)
        raise Exception(msg)

    scope.output.write('    }\n')  # Pulse

    scope.output.write('  }\n')  # Excitation


def write_portmode_loading(scope, att):
    scope.output.write('  Type: PortModeLoading\n')
    indent2 = '  '
    indent4 = '    '
    indent6 = '      '
    indent8 = '        '
    #CardFormat('ScalingFactor', 'ScalingFactor', '%g').write(scope, att, indent2)
    scope.output.write('  Port: {\n')

    port_item = att.findModelEntity('BoundaryPort')
    port_ids = utils.get_entity_ids(scope, port_item)
    if len(port_ids) == 0:
        msg = 'No BoundaryPort specified for Loading %s' % att.name()
        scope.logger.addError(msg)
        raise Exception(msg)
    scope.output.write('    ReferenceNumber: %d\n' % port_ids[0])
    CardFormat('Origin', 'Origin', '%g').write(scope, att, indent4)
    CardFormat('XDirection', 'XDirection', '%g').write(scope, att, indent4)
    CardFormat('YDirection', 'YDirection', '%g').write(scope, att, indent4)

    scope.output.write('    ESolver: {\n')
    # esolver_item = att.findGroup('ESolver')
    # scope.output.write('      Type: %s\n' % )
    CardFormat('Type', 'ESolver', '%s').write(scope, att, indent6)

    scope.output.write('      Mode: {\n')
    CardFormat('ExFile', 'ESolver/ExFile', '%s').write(scope, att, indent8)
    CardFormat('EyFile', 'ESolver/EyFile', '%s').write(scope, att, indent8)
    CardFormat('BxFile', 'ESolver/BxFile', '%s').write(scope, att, indent8)
    CardFormat('ByFile', 'ESolver/ByFile', '%s').write(scope, att, indent8)

    scope.output.write('      }\n')  # Mode
    scope.output.write('    }\n')  # ESolver
    scope.output.write('  }\n')  # Port
