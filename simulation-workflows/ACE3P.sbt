<?xml version="1.0"?>
<SMTK_AttributeResource Version="4">
  <!--**********  Category and Analysis Information ***********-->
  <Categories>
    <Cat>Omega3P</Cat>
    <Cat>S3P</Cat>
    <Cat>T3P</Cat>
    <Cat>Track3P</Cat>
    <Cat>Track3P-Multipacting</Cat>
    <Cat>Track3P-DarkCurrent</Cat>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Harmonic</Cat>
    <Cat>TEM3P-Elastic</Cat>
    <Cat>TEM3P-Thermal</Cat>
    <Cat>TEM3P-Linear-Thermal</Cat>
    <Cat>TEM3P-Nonlinear-Thermal</Cat>
    <Cat>Rf-Postprocess</Cat>
	<Cat>Mesh-Convert</Cat>
	<Cat>Mesh-Check</Cat>
	<Cat>Mesh-Stats</Cat>
  </Categories>

  <Analyses Exclusive="true" Label="Module">
    <Analysis Type="Omega3P">
      <Cat>Omega3P</Cat>
    </Analysis>
    <Analysis Type="S3P">
      <Cat>S3P</Cat>
    </Analysis>
    <Analysis Type="T3P">
      <Cat>T3P</Cat>
    </Analysis>

    <Analysis Type="Track3P" Exclusive="true">
      <Cat>Track3P</Cat>
    </Analysis>
    <Analysis Type="Multipacting" BaseType="Track3P">
      <Cat>Track3P-Multipacting</Cat>
    </Analysis>
    <Analysis Type="Dark Current" BaseType="Track3P">
      <Cat>Track3P-DarkCurrent</Cat>
    </Analysis>

    <Analysis Type="TEM3P" Exclusive="true" />
    <Analysis Type="Elastic Eigenmode" BaseType="TEM3P">
      <Cat>TEM3P-Eigen</Cat>
    </Analysis>
    <!-- <Analysis Type="Harmonic Response" BaseType="TEM3P">
      <Cat>TEM3P-Harmonic</Cat>
    </Analysis> -->
    <Analysis Type="Thermal/Elastic" BaseType="TEM3P">
    </Analysis>
    <Analysis Type="Elastic" BaseType="Thermal/Elastic">
      <Cat>TEM3P-Elastic</Cat>
    </Analysis>
    <Analysis Type="Thermal" BaseType="Thermal/Elastic" Exclusive="true">
      <Cat>TEM3P-Thermal</Cat>
    </Analysis>
    <Analysis Type="Linear" BaseType="Thermal">
      <Cat>TEM3P-Linear-Thermal</Cat>
    </Analysis>
    <Analysis Type="Nonlinear"  BaseType="Thermal">
      <Cat>TEM3P-Nonlinear-Thermal</Cat>
    </Analysis>

    <Analysis Type="ACDTool" Exclusive="true" />
    <Analysis Type="RF Postprocess" BaseType="ACDTool">
      <Cat>Rf-Postprocess</Cat>
    </Analysis>
    <Analysis Type="Mesh Convert" BaseType="ACDTool">
	  <Cat>Mesh-Convert</Cat>
    </Analysis>
    <Analysis Type="Mesh Check" BaseType="ACDTool">
	  <Cat>Mesh-Check</Cat>
    </Analysis>
    <Analysis Type="Mesh Stats" BaseType="ACDTool">
	  <Cat>Mesh-Stats</Cat>
    </Analysis>
  </Analyses>

  <!--**********  Include files ***********-->
  <Includes>
    <File>internal/templates/beamloading.sbt</File>
    <File>internal/templates/boundarycondition.sbt</File>
    <File>internal/templates/loading.sbt</File>
    <File>internal/templates/material.sbt</File>
    <File>internal/templates/monitor.sbt</File>
    <File>internal/templates/analysis.sbt</File>
    <File>internal/templates/track3p.sbt</File>
    <File>internal/templates/tem3p.sbt</File>
    <File>internal/rfpost/rfpost.sbt</File>
  </Includes>

  <Definitions>
    <AttDef Type="InputData">
      <ItemDefinitions>
        <String Name="Source" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>TEM3P-Elastic</Cat>
            <Cat>TEM3P-Thermal</Cat>
          </Categories>
          <ChildrenDefinitions>
            <String Name="NERSCDirectory" Label="Path">
              <BriefDescription>Full path on NERSC Community File System (CFS)</BriefDescription>
            </String>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="NERSC Directory">NERSCDirectory</Value>
              <Items>
                <Item>NERSCDirectory</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>

        <!-- Non-optional version -->
        <String Name="NERSCDataDirectory" Label="NERSC Data Directory">
          <BriefDescription>Full path on NERSC Community File System (CFS)</BriefDescription>
          <Categories>
            <Cat>Track3P</Cat>
          </Categories>
        </String>

        <!-- Results input (path) for RF postprocessing -->

        <!-- Deformed mesh for Omega3P analysis -->
        <String Name="NERSCMeshPath" Label="Deformed Mesh File" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Full path to ncdf file on NERSC Community File System (CFS)</BriefDescription>
          <Categories>
            <Cat>Omega3P</Cat>
          </Categories>
        </String>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="RfPostSourceData" Label="ResultDir">
      <BriefDescription>EM results directory to postprocess</BriefDescription>
        <Categories>
          <Cat>Rf-Postprocess</Cat>
        </Categories>
      <ItemDefinitions>
        <String Name="NERSCRfpostPath" Label="ResultDir">
          <BriefDescription>Full path to EM results directory on NERSC Community File System (CFS)</BriefDescription>
        </String>
      </ItemDefinitions>
    </AttDef>

  </Definitions>

  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Group" Title="ACE3P" TopLevel="true" TabPosition="North"
    FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <View Title="Modules" />

        <!-- O3P/S3P/T3P -->
        <View Title="EMBC" />
        <View Title="EM Materials" />
        <View Title="EM Analysis" />

        <!-- T3P -->
        <View Title="Beam Info" />
        <View Title="Loading" />
        <View Title="Monitors" />

        <!-- Track3P -->
        <View Title="Domain" />
        <View Title="Field"/>
        <View Title="Particles"/>
        <View Title="Track3P Material Model" />
        <View Title="Postprocess"/>

        <!-- TEM3P -->
        <View Title="TEM3P Elastic" />
        <View Title="TEM3P Thermal" />

        <!-- RF PostProcessing (ACDTool) -->
        <View Title="RF Postprocess" />
      </Views>
    </View>

    <View Type="Group" Name="Modules" Label="Modules" Style="Tiled">
      <Views>
        <View Title="SelectModules" />
        <View Title="Results Data" />
      </Views>
    </View>

    <View Type="Analysis" Name="SelectModules" Label=" "
      AnalysisAttributeName="analysis" AnalysisAttributeType="analysis" AnalysisAttributeLabel="Module">
    </View>

    <View Type="Instanced" Title="Results Data" Label="NERSC File System">
      <InstancedAttributes>
        <Att Type="InputData" Name="InputData">
          <ItemViews>
            <View Path="/Source/NERSCDirectory" Type="NERSCDirectory" />
            <View Item="NERSCDataDirectory" Type="NERSCDirectory" />
            <View Item="NERSCRfpostPath" Type="NERSCDirectory" Upstream="false" />
            <View Item="NERSCMeshPath" Type="NERSCDirectory" JobButton="deformed" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <!-- EM Views (O3P, S3P, T3P) -->
    <View Type="Group" Title="EMBC"  Label="Boundary Conditions" Style="Tiled">
      <Views>
        <!-- Hide HFormulation per SLAC request -->
        <!--View Title="HFormulation" / -->
        <View Title="Surface Properties" />
      </Views>
    </View>
    <View Type="Instanced" Title="HFormulation">
      <InstancedAttributes>
        <Att Name="HForumulation" Type="HFormulation" />
      </InstancedAttributes>
    </View>

    <View Type="ModelEntity" Title="Surface Properties" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="SurfaceProperty" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="EM Materials" Label="Materials" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="Material" />
      </AttributeTypes>
    </View>

    <View Type="Group" Title="EM Analysis" Label="Analysis" Style="Tiled">
      <Views>
        <!--View Title="Tolerant" /-->
        <View Title="Finite Element" />
        <View Title="Moving Window" />
        <View Title="Frequency Information" />
        <View Title="EigenSolver" />
        <View Title="Post Process" />
        <View Title="High Order Regions" />
        <View Title="CheckPoint" />
      </Views>
    </View>
    <View Type="Instanced" Title="Tolerant">
      <InstancedAttributes>
        <Att Name="Tolerant" Type="Tolerant" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Finite Element">
      <InstancedAttributes>
        <Att Name="Finite Element Info" Type="FEInfo" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Moving Window">
      <InstancedAttributes>
        <Att Name="MovingWindow" Type="MovingWindow" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Frequency Information">
      <InstancedAttributes>
        <Att Name="EigenSolver" Type="FrequencyInfo" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="EigenSolver">
      <InstancedAttributes>
        <Att Name="Omega3PSolver" Type="Omega3PSolver" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Post Process">
      <InstancedAttributes>
        <Att Name="PostProcess" Type="PostProcess" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="High Order Regions" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="RegionHighOrder" />
      </AttributeTypes>
    </View>

    <!-- T3P Specific Views -->
    <View Type="Instanced" Title="CheckPoint">
      <InstancedAttributes>
        <Att Name="CheckPoint" Type="CheckPoint" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Beam Info" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="BeamLoading" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="Loading" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="Loading" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="Monitors" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="Monitor" />
      </AttributeTypes>
    </View>
    <!-- Track3P-specific views are in templates/track3p.sbt -->

    <!-- TEM3P -->
    <View Type="Group" Title="TEM3P Elastic" TabPosition="North">
      <Views>
        <View Title="TEM3P Mechanical BC" />
        <View Title="TEM3P Elastic Material" />
        <View Title="TEM3P Elastic Analysis"/>
        <View Title="Mesh Output" />
      </Views>
    </View>

    <View Type="Group" Title="TEM3P Thermal" TabPosition="North">
      <Views>
        <View Title="TEM3P Thermal BC" />
        <View Title="Thermal Shells" />
        <View Title="TEM3P Thermal Material" />
        <View Title="Heat Sources" />
        <View Title="TEM3P Thermal Analysis"/>
      </Views>
    </View>
    <!-- TEM3P internal views are in templates/tem3p.sbt -->

    <View Type="Instanced" Title="RF Postprocess">
      <InstancedAttributes>
        <Att Type="RfPostSourceData" Name="RfPostSourceData">
          <ItemViews>
            <View Item="NERSCRfpostPath" Type="NERSCDirectory" JobButton="emag" Upstream="false" DirectoryOnly="true" />
          </ItemViews>
        </Att>
        <Att Name="RfPostprocess" Type="RfPostprocess" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
