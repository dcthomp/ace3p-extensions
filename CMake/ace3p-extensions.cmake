# A contract file for testing ACE3P extensions within SMTK.

cmake_minimum_required(VERSION 3.12)
project(ace3p-extensions)

include(ExternalProject)

ExternalProject_Add(ace3p-extensions
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/ace3p-extensions.git"
  GIT_TAG "origin/master"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_BEFORE_INSTALL True
)
