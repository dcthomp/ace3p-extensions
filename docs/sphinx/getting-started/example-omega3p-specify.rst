Specifying an Omega3P Analysis
==============================

You can use :program:`ModelBuilder for ACE3P` to specify simulation inputs for all of the major codes in the ACE3P Suite: :program:`Omega3P`, :program:`S3P`, :program:`T3P`, :program:`Track3P`, :program:`TEM3P`, and :program:`ACDTool`. In this example, we will setup an :program:`Omega3P` analysis to compute the first three resonant modes for the pillbox geometry.


.. rst-class:: step-number

3\. Set the Analysis Type to Omega3P

In the :guilabel:`Modules` tab, open the drowdown list to the right of the :guilabel:`Analysis` label and select :guilabel:`Omega3P`. In response, :program:`ModelBuilder` will open additonal tabs to the right of :guilabel:`Modules`. If necessary, resize the sidebar panel larger to see the :guilabel:`Boundary Conditions`, :guilabel:`Materials`, and :guilabel:`Analysis` tabs.


.. rst-class:: step-number

4\. Assign Boundary Conditions

Click on the :guilabel:`Boundary Conditions` tab to see a two-column table of the side sets in the model. In the right-hand column, set the surface type to :guilabel:`Magnetic` for side set ID 1 and 2, and :guilabel:`Exterior` for side set ID 6. (Double-click the :guilabel:`Please Select` text to open a drop down list, then select from this list.) When you are done, the tab should look like this.

.. image:: images/project-bcs.png
    :align: center

|

.. rst-class:: step-number

5\. Specify the Number of Eigenmodes

Next click on the :guilabel:`Analysis` tab to view solver settings and options. You can use the default values, but for this walkthrough, we are going to increase the number of eigenmodes to be calculated. You can set the field labeled :guilabel:`Number of eigenmodes searched` to any positive integer -- for this exercise, change it from 1 to 3.


.. rst-class:: step-number

6\. Save the Project

At this point, we have entered enough information for an :program:`Omega3P` analysis, so go to the :guilabel:`ACE3P` menu and select the :guilabel:`Save Project` item.
