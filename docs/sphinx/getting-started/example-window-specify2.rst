Specify Track3P Analysis (part 1)
=================================

Next, we will specify the analysis parameters for the second (:program:`Track3P`) stage in the :guilabel:`Attribute Editor`. In the :guilabel:`Analysis Stages` panel, make sure the :program:`Track3P` stage is selected.


.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Set the Modules Tab to Track3P/Multipacting

In the :guilabel:`Attribute Editor` panel, select the :guilabel:`Modules` tab, and in that tab set the :guilabel:`Analysis` to ``Track3P``, and then set the :guilabel:`Track3P` field to ``Multipacting``.

.. rst-class:: step-number

2\. Set the S3P Results Data Path

Below that is a field labeled :guilabel:`NERSC Data Directory`. This is for specifying the EM data that :program:`Track3P` will use to compute particle trajectories. For this example, we are going to set it to the :program:`S3P` results computed in the first project stage. To do that, click the up-arrow button next to the :guilabel:`NERSC Data Directory` label.

.. image:: images/window-modules2a-annotated.png
    :align: center

|

Modelbuilder will open a dialog listing all jobs from previous project stages. In this case, there is one row in the list, the S3P job you ran previously. Select that row and then click the :guilabel:`Select` button.

.. image:: images/upstream-job-dialog.png
    :align: center

|

Modelbuilder will close the dialog and enter the path to the results directory for the selected job.

.. image:: images/window-modules2.png
    :align: center

|

.. note::
    In practice, you can enter any EM results data computed by :program:`ACE3P` for the ``Window.ncdf`` mesh, whether done as part of a modelbuilder project or not. The :guilabel:`Browse` button is provided to let you navigate to paths outside the current project.
