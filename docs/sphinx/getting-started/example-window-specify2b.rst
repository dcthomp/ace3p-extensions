Specify Track3P Analysis (part 2)
=================================

Continue specifying the rest of the :program:`Track3P` simulation.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

3\. Analysis Tab

Input the following parameters:

a. In the :guilabel:`Total Particle Tracking Time` field, enter ``2``.
b. In the :guilabel:`Localize Bins` field, enter ``128``.
c. Check the :guilabel:`Solid Region` checkbox, and select ``volume 2`` from the drop-down list.
d. Check the :guilabel:`Vacuum Region` checkbox, and select ``volume 1`` from the drop-down list.

.. image:: images/window-analysis2.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. Field Scaling Tab

Input the following parameters:

a. Set :guilabel:`Type` to ``InputPortPower - W``.
b. Set :guilabel:`Scan Token` to ``0: Single Field Level``.
c. In the :guilabel:`Scale` field, enter ``8.0e6``.

.. image:: images/window-field-scaling.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

5\. Emitter Tab

This tab has two sections labeled :guilabel:`Particles Trajectories` and :guilabel:`Emitters`, respectively. In the :guilabel:`Emitters` section, we will be creating two Emitter instances, both assigned to surface number 6.

**Create the first Emitter** by clicking the :guilabel:`New` button. Set these parameters:

a. Set :guilabel:`Type` to ``5: Solid Electronic Surface``.
b. Set the :guilabel:`Region Bounds Min (m)` parameters to: ``0.0``   ``0.0``  ``-0.002``.
c. Set the :guilabel:`Region Bounds Max (m)` parameters to: ``0.045`` ``0.045`` ``0.0``.
d. Assign ``surface 6`` to the :guilabel:`Current` list at the bottom of the panel.

**Create the second Emitter** by clicking the :guilabel:`New` button and set its parameters as follows:

a. Set :guilabel:`Type` to ``2: Electronic Surface``.
b. Set the :guilabel:`Region Bounds Min (m)` parameters to: ``0.001`` ``0.001``  ``0.0``.
c. Set the :guilabel:`Region Bounds Max (m)` parameters to: ``0.045`` ``0.045`` ``0.06``.
d. Assign ``surface 6`` to the :guilabel:`Current` list at the bottom of the panel.

.. image:: images/window-emitter.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

6\. Material Tab

For this example, you will create four Material attributes. In the :guilabel:`Material` tab, to the left of the :guilabel:`New` button there is a drop-down control for selecting one of three material types: (i) Absorber, (ii) Primary, (iii) Secondary. To create each material, first select the type from the drop-down control, then press the :guilabel:`New` button.

**The first material specifies surface number 6 as both a primary and secondary particle emitter.**

a. Select :guilabel:`Primary` from the drop-down control, then click the :guilabel:`New` button.
b. Check the checkbox labeled :guilabel:`Seconday` (below the material list).
c. Below that, move ``surface 6`` to the :guilabel:`Current` list.

**The second material specifies the remaing surfaces as particle absorbers.**

a. Select :guilabel:`Absorber` from the drop-down control, then click the :guilabel:`New` button.
b. Move ``surface 1``, ``surface 2``, ``surface 7``, ``surface 8`` to the :guilabel:`Current` list at the bottom of the panel.

.. tip::

    You can select and move more than one option at a time in the :guilabel:`Current` and :guilabel:`Available` lists.

**The third material specifes volume number 1 (cavity) as a secondary particle emitter.**

a. Select :guilabel:`Secondary` from the drop-down control, then click the :guilabel:`New` button.
b. Move ``volume 1`` to the :guilabel:`Current` list at the bottom of the panel.

**The fourth material specifies volume number 2 (window) as a primary particle emitter.**

a. Select :guilabel:`Primary` from the drop-down control, then click the :guilabel:`New` button.
b. Move ``volume 2`` to the :guilabel:`Current` list at the bottom of the panel.

.. image:: images/window-material2.png
    :align: center

|
