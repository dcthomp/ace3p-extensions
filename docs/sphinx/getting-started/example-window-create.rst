Create Window Project
=====================

The first step is to create a new project. Do this by going to the :guilabel:`ACE3P` menu and selecting the :guilabel:`Create Project...` item, which opens the :guilabel:`Create ACE3P Project` dialog. For the :guilabel:`Project Name` you can use the default provided or enter a more descriptive name such as "window-example".

For this example, you will be using the ``Window.ncdf`` file from the :program:`ACE3P` code workshops. You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632>`_ if needed. Enter this in the :guilabel:`Mesh File` page of the dialog.

.. note::
    When using the :guilabel:`Browse...` option to enter ``Window.ncdf``, you will need to change the file visibility option to see files with the ``ncdf`` extension. Locate the :guilabel:`Files of type:` field next to a dropdown list. In the dropdown list, select either ``NetCDF Files (*.ncdf)`` or ``All Files (*)``.

.. image:: images/window-create-project.png
    :align: center

|
