Preview Command File Before Job submit
---------------------------------------

In the future, there might be situations in which new features are added to the :program:`ACE3P` simulation codes before being incorporated into :program:`ModelBuilder`. To deal with those cases, :program:`modelbuilder` includes an option to display the :program:`ACE3P` command file in a text editor before uploading it to NERSC. To enable this option, go to the :guilabel:`Edit` menu and select the :guilabel:`Settings...` item to open the settings dialog (on macOS systems, go to the :guilabel:`modelbuilder` menu and select the :guilabel:`Preferences...` item). In the settings dialog, go to the :guilabel:`ACE3P` tab and click the checkbox labeled :guilabel:`Enable editing of job input before submission`. Then click the :guilabel:`Apply` button and the :guilabel:`OK` button to close the dialog.

.. image:: ../images/settings-preview.png
    :align: center

|

When that option is enabled, the process for submitting an analysis job to NERSC is modified. After setting the job properties (number of nodes, number of cores, etc.) in the job-submit dialog, :program:`modelbuilder` generates the :program:`ACE3P` command file and displays it in a text-editor dialog. Users can make any changes to the file and then click one of the buttons to :guilabel:`Cancel Submission`, :guilabel:`Submit Without Edits`, or :guilabel:`Submit With Edits`.

.. image:: ../images/modelbuilder-preview.png
    :align: center

|
