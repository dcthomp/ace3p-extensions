Start ParaView Server Manually
-------------------------------

As described in :ref:`Example: Remote Visualization`, modelbuilder's :guilabel:`Simulation Jobs` panel provides a tool button for launching a :program:`ParaView Server` session on NERSC for remote visualization of results data from jobs submitted by modelbuilder. To visualize other data on NERSC (that are not part of any modelbuilder project) the :guilabel:`ACE3P` menu also includes a submenu labeled :guilabel:`Launch Remote ParaView Server`. That submenu provides a list of machines for connecting remotely (At present, :program:`ParaView Server` is only available on  ``Cori``.) When a machine is selected, :program:`modelbuilder` will open the connection dialog for starting :program:`ParaView Server` on the remote machine.

.. image:: ../images/ace3p-menu-update.png
    :align: center

|

Once :program:`ParaView Server` has started and connects to modelbuilder, you can load any dataset on the remote machine. You can enable the :guilabel:`ParaView` features from the toolbar or use the :guilabel:`SLACTools` loader.
