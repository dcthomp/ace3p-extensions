View Postprocess Results
========================

Once the :program:`rf-postprocessing` job has completed, you may view the results directly in ModelBuilder for :program:`ACE3P`.

If the :guilabel:`NERSC File Browser` panel is not visible, select the :guilabel:`View` menu and click on the :guilabel:`NERSC File Browser` option to bring the panel into ModelBuilder. You can navigate manually to the :program:`rf-postprocessing` remote directory from within the :guilabel:`NERSC File Browser` panel, however, there is a shortcut button available from the Simulation Jobs panel. From the Jobs panel, select the ACDTool job just run, and select the right arrow button on the details panel next to the Remote Directory field. This will automatically navigate the :guilabel:`NERSC File Browser` to the remote directory of the :program:`rf-postprocessing` job.

.. note::
    The right arrow button in the jobs details panel, and the functionality of the :guilabel:`NERSC File Browser` will only be available if the user is logged in to NERSC.

Having navigated to the :program:`rf-postprocessing` job remote directory, you may review the output file titled ``rfpost.out`` by right clicking on the file in the :guilabel:`NERSC File Browser` and selecting :guilabel:`View as Text`. In response, Modelbuilder downloads the file from NERSC and displays its contents in a popup dialog.

.. image:: ../images/rfPost-outText.jpg
    :align: center
