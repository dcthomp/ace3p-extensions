Run S3P Analysis
================

You are now ready to submit the S3P analysis job. The steps are the same as described in :ref:`Running Omega3P on NERSC` and :ref:`Tracking Job Status`. To start, make sure you are logged into NERSC, then open the :guilabel:`ACE3P` menu and select the :guilabel:`Submit Analysis Job...` item to open the job-submit dialog. Enter your :guilabel:`Project repository`; you can use the default values for the rest of the fields in the dialog.

Once you submit the job, :program:`ModelBuilder for ACE3P` will add a row for it in the :guilabel:`Simulation Jobs` panel and begin polling NERSC for status updates.
