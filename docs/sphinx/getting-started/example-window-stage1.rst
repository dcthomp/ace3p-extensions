Stage 1: S3P
============

For the first stage, you will specify the S3P analysis parameters, submit the analysis job to NERSC, and then view the results.

.. toctree::
    :maxdepth: 1

    example-window-specify1.rst
    example-window-submit1.rst
    example-window-view1.rst
