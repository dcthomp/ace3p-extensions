Disconnecting
=============

.. rst-class:: step-number

9\. When You Are Done

When you are done, click the :guilabel:`Disconnect` tool button to end the visualization session. :program:`ModelBuilder for ACE3P` will close the remote tab and close the :program:`ParaView` server running on NERSC.

.. image:: ../images/disconnect-button.png
  :align: center

|

.. important::

  Be sure to disconnect the remote :program:`ParaView` server before the the time limit option (:guilabel:`job wall time`) is reached. Otherwise the :program:`ParaView` server instance(s) running on NERSC will automatically terminate, closing the remote connection. When that happens, the behavior of :program:`ModelBuider for ACE3P` will be undefined and could be unstable.
