Configuring a Remote Visualization Session
==========================================

.. rst-class:: step-number

1\. Open the Omega3P Project

From the :guilabel:`ACE3P` menu, use either the :guilabel:`Open Project` or :guilabel:`Recent Projects` item to load the :program:`Omega3P` example project

.. rst-class:: step-number

2\. Select the Omega3P Job

If the :guilabel:`Simulation Jobs` panel is not displayed, use the :guilabel:View menu to make it visible. In the list of jobs select the analysis job run previously, so that you can see the jobs details in the lower half of the widget.

.. image:: ../images/remote-viz-begin.png
    :align: center

.. rst-class:: step-number

3\. Open the Connection Options Dialog

Locate the |paraview-icon| (:program:`ParaView`) tool button to the right of the :guilabel:`Remote Directory` field near the bottom of the :guilabel:`Simulation Jobs` panel.

.. |paraview-icon| image:: ../../_static/images/pvIcon.svg
  :width: 16

.. image:: ../images/remote-viz-button.png
    :align: center
    :width: 40%

Click the |paraview-icon| tool button (to the right of the :guilabel:`Remote Directory` field) to open a :guilabel:`Connection Options for "NERSC Cori"` dialog.

.. image:: ../images/connection-options.png
    :align: center
    :width: 50%

.. rst-class:: step-number

4\. Configure Options

Update these fields in the dialog:

=======================  =========================
**user name**            <your NERSC user name>
**number of processes**      1
**job wall time**        00:15:00
**account**              <your project repository>
**queue**                debug
=======================  =========================

When you done, click :guilabel:`OK` button, which will open an SSH terminal for starting the connection.
