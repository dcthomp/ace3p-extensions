Example: Multiple Stages (Rf Gun)
==================================

.. image:: images/rfgun-result2.png

This example creates a multiple-stage :program:`ACE3P` project to carry out RF and thermal/elastic analyses of an NC RF gun coupler cell, to determine the structural deformation caused by electromagnetic heating (which can reduce operating efficiency). The first stage of the project computes the resonant frequency of the cell cavity and corresponding electromagnetic fields using :program:`Omega3P`. The second stage then computes the physical distortion (nodal displacement) of the cell structure using :program:`TEM3P` with the results computed in the first stage.

In this tutorial you will create a new project, specify the analysis parameters for the first stage (:program:`Omega3P`), run an analysis job on NERSC, and then visualize the results. You will then add a second stage to the project and run through the specify/run/visualize steps to compute the temperature fields and mesh distortion due to thermal effects (:program:`TEM3P`). Following this, you will add a third stage and run through the specify/run/visualize steps to compute the resonant data for the deformed cavity mesh (:program:`Omega3P`). Before beginning this tutorial, it will be helpful to have first gone through the :ref:`Example: Multiple Stages (Window)`.

.. toctree::
    :maxdepth: 1

    rfgun/example-rfgun-create.rst
    rfgun/example-rfgun-specify1.rst
    rfgun/example-rfgun-stage2.rst
    rfgun/example-rfgun-stage3.rst

.. note::
    Screenshots in this section were taken from Modelbuilder running on Windows 11.
