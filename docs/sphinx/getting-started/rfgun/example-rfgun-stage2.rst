Stage 2: TEM3P
===============

Next we will add a second stage to the project for computing a TEM3P simulation using the results of the Omega3P analysis. After creating the stage, you will specify the TEM3P thermoelastic simulation, submit the analysis job to NERSC, and then view the results.

.. toctree::
    :maxdepth: 1

    example-rfgun-addstage.rst
    example-rfgun-specify2.rst
    example-rfgun-specify2b.rst
    example-rfgun-submit2.rst
