Create Stage 2 (TEM3P)
=======================

.. rst-class:: step-number

1\. New Stage

To add a second stage to the project, go to the :guilabel:`ACE3P` menu and select the :guilabel:`Add Stage...` item to open the dialog for specifying the new stage. In the dialog, set the :guilabel:`Mesh` field to ``Import New Mesh File``. For the :guilabel:`Mesh File` field, we are going to use the ``RfGunBody.gen`` file from the :program:`ACE3P` code workshops. You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632>`_.

For a more detailed description of adding a stage to a project, refer to :ref:`Create Stage 2 (Track3P)`.

.. image:: ../images/rfgun-add-new-stage.png
    :align: center

|

.. rst-class:: step-number

2\. Modules Tab

To set up the analysis, select the :guilabel:`Modules` tab and set the :guilabel:`Analysis` to ``TEM3P``.  For the :guilabel:`TEM3P` type, select ``Thermal/Elastic``, and then check both checkboxes for :guilabel:`Elastic` and :guilabel:`Thermal`.  Next to the :guilabel:`Thermal` checkbox, select ``Linear`` from the drop-down menu.

Below that, check the :guilabel:`Source` checkbox to display a new input field for the path to the EM field data to use. Click the up-arrow button next to the :guilabel:`Path` label to open a dialog listing jobs run for the first stage. In the dialog, select the one row in the list for :program:`Omega3P` job previously, then click the :guilabel:`Select` button. For a more detailed description, refer back to the :ref:`Window example<Specify Track3P Analysis (part 1)>` (Modules Tab section).

.. image:: ../images/rfgun-modules2.png
    :align: center

|
