Specify TEM3P - Elastic
========================

Next, we will specify the elastic analysis parameters in the :guilabel:`Attribute Editor`.


.. rst-class:: step-number

1\. TEM3P Elastic - Boundary Conditions Tab

Select the :guilabel:`TEM3P Elastic` tab next and then select its :guilabel:`Boundary Conditions` tab. Set the elastic boundary condition types and data items per this table and screenshot:

::

  Side Set   Type          Item Name             Item Value
  ------------------------------------------------------------------------
      1      Mixed         Mixed Type (xyz)    Neumann, Nuemann, Dirichlet
                           Mixed Value (xyz)   0, 0, 0
  ------------------------------------------------------------------------
      2      Mixed         Mixed Type (xyz)    Dirichlet, Neumann, Nuemann
                           Mixed Value (xyz)   0, 0, 0
  ------------------------------------------------------------------------
      3      Mixed         Mixed Type (xyz)    Neumann, Dirichlet, Nuemann
                           Mixed Value (xyz)   0, 0, 0
  ------------------------------------------------------------------------
      4     Neumann        Normal Loading      0
  ------------------------------------------------------------------------
      5     Neumann        Normal Loading      0
  ------------------------------------------------------------------------
      6     LF Detuning    use the values shown in the screenshot below
  ------------------------------------------------------------------------


.. image:: ../images/rfgun-boundary-conditions2a.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

2\. TEM3P Elastic - Materials Tab

In the :guilabel:`TEM3P Elastic` tab select its :guilabel:`Materials` tab. Add a material attribute by clicking the :guilabel:`New` button, set the :guilabel:`Material` to ``Copper`` in the drop-down list, and assign ``Unnamed Block ID: 1`` to the :guilabel:`Current` list.

.. image:: ../images/rfgun-materials2a.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

3\. TEM3P Elastic - Analysis Tab

In the :guilabel:`Analysis` tab, set the elastic solver parameters as shown in this screenshot:

.. image:: ../images/rfgun-analysis2a.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. TEM3P Elastic - Mesh Output Tab

In the :guilabel:`Mesh Output` tab, set the parameters as shown in this screenshot:

.. image:: ../images/rfgun-mesh-output2a.png
    :align: center

|
