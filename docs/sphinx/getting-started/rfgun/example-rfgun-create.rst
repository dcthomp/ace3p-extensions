Create RfGun Project
====================

Start by going to the :guilabel:`ACE3P` menu and selecting the :guilabel:`New Project…` item to open the new-project dialog. In the first page of the dialog, set the :guilabel:`Project Name` to ``rfgun`` (or some other descriptive name) and click the :guilabel:`Next` button. In the next page, load the mesh file representing the Rf gun cavity. For this example, we are using the ``RfGunVacuum.gen`` file from the :program:`ACE3P` code workshops. You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632>`_.

.. image:: ../images/rfgun-create-project.png
    :align: center

|
