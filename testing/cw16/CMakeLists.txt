
set(smtk_pythonpath ${smtk_DIR}/${SMTK_PYTHONPATH})
if(NOT IS_ABSOLUTE ${smtk_pythonpath})
  get_filename_component(smtk_pythonpath
    ${PROJECT_BINARY_DIR}/${smtk_DIR}/${SMTK_PYTHON_MODULEDIR} ABSOLUTE)
endif()

set(pyenv
  ${PROJECT_BINARY_DIR}
  ${smtk_pythonpath}
  $ENV{PYTHONPATH})
if (WIN32)
  string(REPLACE ";" "\;" pyenv "${pyenv}")
else ()
  string(REPLACE ";" ":" pyenv "${pyenv}")
endif()

set(pathenv)
if (WIN32)
  set(pathenv
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
    $ENV{PATH})
  string(REPLACE ";" "\;" pathenv "${pathenv}")
endif()

# Function to set test properties
function (config_test test_name)
  set_tests_properties("${test_name}"
    PROPERTIES
      ENVIRONMENT "PYTHONPATH=${pyenv}"
      LABELS "CW16"
  )
  if (pathenv)
    set_property(TEST "${test_name}" APPEND
      PROPERTY
        ENVIRONMENT "PATH=${pathenv}"
    )
  endif ()
endfunction()

# Create directory for test files
set(dat_dir "${CMAKE_BINARY_DIR}/Testing/Temporary/cw16/files")
file(MAKE_DIRECTORY "${dat_dir}")

# Add static files to use in tests
file(TOUCH "${dat_dir}/copper.dat")
file(TOUCH "${dat_dir}/al300")
file(TOUCH "${dat_dir}/HighRRRNb")
file(TOUCH "${dat_dir}/SRinnerCU")

# TEM3P
add_test (
  NAME cw16_tem3p_RfGunBody
  COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/cw16test.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../simulation-workflows/ACE3P.sbt"
    "${CW16_PATH}/examples/tem3p/RfGun-Coupler/RfGunBody.ncdf"
    "${CMAKE_CURRENT_SOURCE_DIR}/tem3p/RfGun-Coupler/RfGunBody.yml"
    -o "${CMAKE_BINARY_DIR}/Testing/Temporary/cw16/tem3p/RfGunBody"
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/Testing"
)
config_test(cw16_tem3p_RfGunBody)

add_test (
  NAME cw16_tem3p_SrfCavity-Coupler_FPC-Body
  COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/cw16test.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../simulation-workflows/ACE3P.sbt"
    "${CW16_PATH}/examples/tem3p/SrfCavity-Coupler/2D-TTF3-FPC-Body.ncdf"
    "${CMAKE_CURRENT_SOURCE_DIR}/tem3p/SrfCavity-Coupler/FPC-Body.yml"
    -o "${CMAKE_BINARY_DIR}/Testing/Temporary/cw16/tem3p/SrfCavity-Coupler_FPC-Body"
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/Testing"
)
config_test(cw16_tem3p_SrfCavity-Coupler_FPC-Body)


# Track3P
add_test (
  NAME cw16_track3p_Pillbox
  COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/cw16test.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../simulation-workflows/ACE3P.sbt"
    "${CW16_PATH}/examples/track3p/Pillbox/Pillbox.gen"
    "${CMAKE_CURRENT_SOURCE_DIR}/track3p/Pillbox/Pillbox.yml"
    -o "${CMAKE_BINARY_DIR}/Testing/Temporary/cw16/track3p/Pillbox"
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/Testing"
)
config_test(cw16_track3p_Pillbox)

add_test (
  NAME cw16_track3p_Tesla-PEmission
  COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/cw16test.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../simulation-workflows/ACE3P.sbt"
    "${CW16_PATH}/examples/track3p/Tesla/Tesla.ncdf"
    "${CMAKE_CURRENT_SOURCE_DIR}/track3p/Tesla/Tesla-PEmission.yml"
    -o "${CMAKE_BINARY_DIR}/Testing/Temporary/cw16/track3p/Tesla-PEmission"
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/Testing"
)
config_test(cw16_track3p_Tesla-PEmission)

add_test (
  NAME cw16_track3p_Tesla-SEmission
  COMMAND "${PYTHON_EXECUTABLE}" "${CMAKE_CURRENT_SOURCE_DIR}/cw16test.py"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../simulation-workflows/ACE3P.sbt"
    "${CW16_PATH}/examples/track3p/Tesla/Tesla.ncdf"
    "${CMAKE_CURRENT_SOURCE_DIR}/track3p/Tesla/Tesla-SEmission.yml"
    -o "${CMAKE_BINARY_DIR}/Testing/Temporary/cw16/track3p/Tesla-SEmission"
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/Testing"
)
config_test(cw16_track3p_Tesla-SEmission)
