# Omega3P Test 1

## Purpose

Verify that a minimal test case can generate an ACE3P input file.

## Prerequisites

[ ] Complete the setup procedures in [setup.md](file:./setup.md)

[ ] Clone the ace3p-extensions repository, either https://gitlab.kitware.com/cmb/plugins/ace3p-extensions.git or git@gitlab.kitware.com:cmb/plugins/ace3p-extensions.git.


## Procedure

[ ] Make sure the ace3p-extensions clone is up to date

* `> git checkout master`
* `> git clean -f -d`
* `> git pull`

In some cases, a different branch of ace3p-extensions might be used for testing. In that case, the alternate branch would be specified in the GitLab issue documenting the test.

[ ] In modelbuilder, select the "Attribute Editor" tab.

[ ] Select the Project => New Project... menu item. Expect a directory browser dialog to open. In that dialog, navigate to the test directory you created in the setup procedure and create a subdirectory `omega3p-test1`. Select the new subdirectory and click the `OK`. Expect modelbuilder to display a "Specify New Project" dialog.

[ ] In the "Specify New Project" dialog, set the "Simulation Template File" to the ACE3P template that is in the ace3p-extensions repository at `simulation-workflows/ACE3P.sbt`. Set the "Geometry File" to `data/model/3d/genesis/pillbox4.gen`.

[ ] In the "Specify New Project" dialog, click the "Apply" button and expect modelbuilder to close the dialog, then display the model geometry in the 3D view and update the Attribute Editor to display a "Modules" tab. On some systems, the model is not immediately displayed, but instead it appears when the "Resources" tab is selected. If that occurs, add a comment to the GitLab issue. Verify that there are two items listed in the "Resources" panel, labeled `sbi.default` and `pillbox4`.

[ ] In the "Attribute Editor" panel, select the "Omega3P" option in the "Modules" tab, and expect modelbuilder to display 3 additional tabs "Boundary Conditions", "Materials", and "Analysis".

[ ] In the "Boundary Conditions" panel:

* Set "Unnamed set ID: 1" to "Magnetic"
* Set "Unnamed set ID: 2" to "Magnetic"
* Set "Unnamed set ID: 6" to "Exterior"

[ ] In the "Analysis" tab:

* Set "Number of eigenmodes searched" to 3. Expect the background color of that field to change from yellow to white.

[ ] Click the Project => Project Save menu item. Expect modelbuilder to display "Saved project" in the "Output Messages" panel.

[ ] Select the Project => Export Project menu item, and expect modelbuilder to open a "Simulation Export Dialog" with every field set and the "Apply" button enabled. Click the "Apply" button and expect modelbuilder to close the dialog and write a few more lines to the "Output Messages" panel, the last one `Number of warnings: 0`.

[ ] Verify that a short text file was created in a project subdirectory `omega3p-test1/export` with an extension `omega3p`. The file should be approximately 250 bytes in size and have approximately 25 lines of text.

[ ] Select the Project => Close Project menu item and expect modelbuilder to clear both tabs ("Attribute Editor", "Resources") and the 3D display.
